import path from 'path'
import { generateTypeScriptTypes } from 'graphql-schema-typescript'
import schema from '../src/schema'

const appDir = path.dirname(path.dirname(require.main.filename))
const outputFileDir = path.join(appDir, 'src', '.generated', 'resolvers.d.ts')
generateTypeScriptTypes(schema, outputFileDir, {
  smartTParent: true,
  smartTResult: true,
  strictNulls: false,
  asyncResult: true,
  requireResolverTypes: false,
  noStringEnum: true
})
  .then(() => {
    console.log(`Generated types in ${outputFileDir}`)
    process.exit(0)
  })
  .catch((err: any) => {
    console.error(err)
    process.exit(1)
  })
