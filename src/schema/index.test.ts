import { expect } from 'chai'
import schema from './'

describe('Graphql schema should be generated', () => {
  it('Should generate a string', () => {
    expect(schema).to.be.a('string')
  })

  it('Should generate a non empty string', () => {
    expect(schema.length).to.above(1)
  })

  it('Should contain types', () => {
    expect(schema).to.contain('Query')
    expect(schema).to.contain('Mutation')
    expect(schema).to.contain('User')
    expect(schema).to.contain('Offer')
    expect(schema).to.contain('Match')
    expect(schema).to.contain('Game')
  })
})

// console.log(schema)
