
const data = `type Mutation {
  addUserGames(input: AddGamesMutationInput!): AddGamesMutationResult
  removeUserGames(input: RemoveGamesMutationInput!): RemoveGamesMutationResult
  updateUserGames(input: UpdateGamesMutationInput!): UpdateGamesMutationResult
  addDbGames(input: AddDbGamesMutationInput!): AddDbGamesMutationResult
  updateOffer(input: UpdateOfferMutationInput): UpdateOfferMutationResult
  createOffer(input: CreateOfferMutationInput): CreateOfferMutationResult
  updateUserInfo(input: UpdateUserInfoMutationInput!): UpdateUserInfoMutationResult
}

type Query {
  searchGames(input: SearchGamesQueryInput!): SearchGamesQueryResult!
  matches(input: MatchQueryInput!): MatchQueryResult!
  offers(input: OfferQueryInput!): OfferQueryResult!
  me: User
  user(input: UserQueryInput!): User
  count: Int
}

type AddGamesMutationResult {
  result: Boolean!
  error: Error
}

input AddGamesMutationInput {
  games: [AddGamesInput!]!
}

type RemoveGamesMutationResult {
  result: Boolean!
}

input RemoveGamesMutationInput {
  games: [RemoveGamesInput!]!
}

type UpdateGamesMutationResult {
  result: Boolean!
}

input UpdateGamesMutationInput {
  games: [UpdateGamesInput!]!
}

type AddDbGamesMutationResult {
  result: Boolean
  notAdded: [AddDbGamesMutationResultNotAdded]
  error: String
}

input AddDbGamesMutationInput {
  games: [AddDbGamesInput!]!
}

type UpdateOfferMutationResult {
  result: Offer
}

input UpdateOfferMutationInput {
  offerId: ID!
  type: UpdateOfferMutationInputType!
}

type CreateOfferMutationResult {
  result: Offer
  error: Error
}

input CreateOfferMutationInput {
  offer: OfferInput!
}

type UpdateUserInfoMutationResult {
  result: UserInfo
  error: Error
}

input UpdateUserInfoMutationInput {
  info: UserInfoInput!
}

type UserGameDetails {
  description: String!
  status: UserGameDetailsStatus!
  tradeType: UserGameDetailsTradeType!
}

type Game {
  name: String!
  consoleType: String
  id: ID!
  imageUrl: String
}

enum ErrorType {
  UPGRADE_MEMBERSHIP
  AUTH_ERROR
  LOCATION_NOT_SET
}

type SearchGamesQueryResult {
  result: [Game!]
}

input SearchGamesQueryInput {
  searchText: String!
  limit: Int
}

type MatchQueryResult {
  result: [Match!]
  error: Error
}

input MatchQueryInput {
  sortBy: MatchSortType!
}

type OfferQueryResult {
  result: [Offer!]
  pageInfo: PageInfo
}

input OfferQueryInput {
  status: OfferStatus!
  pagination: PaginationInput
}

type User {
  id: ID!
  info: UserInfo!
  wantedGames: [UserGame!]
  hasGames: [UserGame!]
}

input UserQueryInput {
  by: UserQueryType!
  value: String!
}

type UserInfo {
  email: String
  description: String
  isPro: Boolean
  epochTimeCreated: String
  location: UserInfoLocation
  noOfSuccessfulExchanges: Int
  rating: UserInfoRating
  userImageUrl: String
  userName: String
  isBanned: Boolean
  setting_matchNotifications: Boolean
  user_subscriptionCancelUrl: String
  user_subscriptionUpdateUrl: String
}

type UserGame {
  id: ID!
  details: UserGameDetails!
  game: Game!
}

enum OfferStatus {
  pending
  ongoing
  completed
  cancelled
  declined
}

type UserInfoLocation {
  country: String!
  state: String!
}

type UserInfoRating {
  negative: Int
  positive: Int
}

type Error {
  id: ID!
  message: String!
  type: ErrorType!
}

input AddGamesInput {
  gameId: ID!
  details: UserGameDetailsInput!
  customItemDetails: AddGamesInputCustomItemDetails
}

input RemoveGamesInput {
  id: ID!
}

input UpdateGamesInput {
  id: ID!
  details: UpdateGamesInputDetails!
}

type AddDbGamesMutationResultNotAdded {
  name: String!
  consoleType: String!
  imageUrl: String!
  popularity: Float!
}

input AddDbGamesInput {
  name: String!
  consoleType: String!
  imageUrl: String!
  popularity: Float!
}

type Offer {
  offerId: ID!
  senderGames: [UserGame!]!
  receiverGames: [UserGame!]!
  receiverId: ID!
  senderId: ID!
  epochTimeCreated: String!
  status: OfferStatus!
  receiverStatus: OfferStatus!
  senderStatus: OfferStatus!
}

enum UpdateOfferMutationInputType {
  complete
  accept
  decline
  cancel
}

input OfferInput {
  myGames: [UserGameInput!]!
  otherGames: [UserGameInput!]!
  otherId: ID!
}

input UserInfoInput {
  email: String
  description: String
  isPro: Boolean
  location: UserInfoLocationInput
  userImageUrl: String
  userName: String
  isBanned: Boolean
  setting_matchNotifications: Boolean
  user_subscriptionCancelUrl: String
  user_subscriptionUpdateUrl: String
}

enum UserGameDetailsStatus {
  has
  want
}

enum UserGameDetailsTradeType {
  swap
  sale
}

type Match {
  id: ID!
  userImageUrl: String!
  userName: String!
  matchRate: Float
  wantedGameNames: [String!]
  hasGameNames: [String!]
  state: String
  country: String
}

enum MatchSortType {
  location
  matchRate
}

type PageInfo {
  noOfItems: Int!
}

input PaginationInput {
  offset: Int!
  limit: Int!
}

enum UserQueryType {
  id
  username
}

input UserGameDetailsInput {
  description: String!
  status: UserGameDetailsStatus
  tradeType: UserGameDetailsTradeType!
}

input AddGamesInputCustomItemDetails {
  name: String!
  consoleType: String!
}

input UpdateGamesInputDetails {
  description: String!
  status: UserGameDetailsStatus!
  tradeType: UserGameDetailsTradeType!
}

input UserGameInput {
  details: UserGameDetailsInput!
  gameId: ID!
}

input UserInfoLocationInput {
  longitude: Float!
  latitude: Float!
}
`
export default data
