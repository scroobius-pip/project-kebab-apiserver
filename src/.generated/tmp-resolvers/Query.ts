// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { QueryResolvers } from "../graphqlgen";

export const Query: QueryResolvers.Type = {
  ...QueryResolvers.defaultResolvers,
  searchGames: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  matches: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  offers: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  me: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  user: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  count: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  }
};
