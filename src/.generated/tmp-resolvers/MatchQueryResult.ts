// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { MatchQueryResultResolvers } from "../graphqlgen";

export const MatchQueryResult: MatchQueryResultResolvers.Type = {
  ...MatchQueryResultResolvers.defaultResolvers,

  result: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  }
};
