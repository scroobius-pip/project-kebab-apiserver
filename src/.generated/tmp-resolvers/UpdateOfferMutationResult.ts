// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { UpdateOfferMutationResultResolvers } from "../graphqlgen";

export const UpdateOfferMutationResult: UpdateOfferMutationResultResolvers.Type = {
  ...UpdateOfferMutationResultResolvers.defaultResolvers
};
