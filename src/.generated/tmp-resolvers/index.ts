// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { Resolvers } from "../graphqlgen";

import { Query } from "./Query";
import { SearchGamesQueryResult } from "./SearchGamesQueryResult";
import { Game } from "./Game";
import { MatchQueryResult } from "./MatchQueryResult";
import { Match } from "./Match";
import { Error } from "./Error";
import { OfferQueryResult } from "./OfferQueryResult";
import { Offer } from "./Offer";
import { UserGame } from "./UserGame";
import { UserGameDetails } from "./UserGameDetails";
import { PageInfo } from "./PageInfo";
import { User } from "./User";
import { UserInfo } from "./UserInfo";
import { UserInfoLocation } from "./UserInfoLocation";
import { UserInfoRating } from "./UserInfoRating";
import { Mutation } from "./Mutation";
import { AddGamesMutationResult } from "./AddGamesMutationResult";
import { RemoveGamesMutationResult } from "./RemoveGamesMutationResult";
import { UpdateGamesMutationResult } from "./UpdateGamesMutationResult";
import { AddDbGamesMutationResult } from "./AddDbGamesMutationResult";
import { AddDbGamesMutationResultNotAdded } from "./AddDbGamesMutationResultNotAdded";
import { UpdateOfferMutationResult } from "./UpdateOfferMutationResult";
import { CreateOfferMutationResult } from "./CreateOfferMutationResult";
import { UpdateUserInfoMutationResult } from "./UpdateUserInfoMutationResult";

export const resolvers: Resolvers = {
  Query,
  SearchGamesQueryResult,
  Game,
  MatchQueryResult,
  Match,
  Error,
  OfferQueryResult,
  Offer,
  UserGame,
  UserGameDetails,
  PageInfo,
  User,
  UserInfo,
  UserInfoLocation,
  UserInfoRating,
  Mutation,
  AddGamesMutationResult,
  RemoveGamesMutationResult,
  UpdateGamesMutationResult,
  AddDbGamesMutationResult,
  AddDbGamesMutationResultNotAdded,
  UpdateOfferMutationResult,
  CreateOfferMutationResult,
  UpdateUserInfoMutationResult
};
