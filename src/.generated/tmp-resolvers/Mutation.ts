// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { MutationResolvers } from "../graphqlgen";

export const Mutation: MutationResolvers.Type = {
  ...MutationResolvers.defaultResolvers,
  addUserGames: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  removeUserGames: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  updateUserGames: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  addDbGames: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  updateOffer: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  createOffer: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  updateUserInfo: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  }
};
