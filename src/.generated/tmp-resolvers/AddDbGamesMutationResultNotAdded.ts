// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { AddDbGamesMutationResultNotAddedResolvers } from "../graphqlgen";

export const AddDbGamesMutationResultNotAdded: AddDbGamesMutationResultNotAddedResolvers.Type = {
  ...AddDbGamesMutationResultNotAddedResolvers.defaultResolvers
};
