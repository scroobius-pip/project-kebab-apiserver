// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { UserInfoLocationResolvers } from "../graphqlgen";

export const UserInfoLocation: UserInfoLocationResolvers.Type = {
  ...UserInfoLocationResolvers.defaultResolvers
};
