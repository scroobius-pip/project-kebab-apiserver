// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { UserInfoResolvers } from "../graphqlgen";

export const UserInfo: UserInfoResolvers.Type = {
  ...UserInfoResolvers.defaultResolvers,

  user_subscriptionCancelUrl: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  user_subscriptionUpdateUrl: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  }
};
