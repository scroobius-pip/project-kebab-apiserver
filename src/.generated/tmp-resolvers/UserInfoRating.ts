// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { UserInfoRatingResolvers } from "../graphqlgen";

export const UserInfoRating: UserInfoRatingResolvers.Type = {
  ...UserInfoRatingResolvers.defaultResolvers
};
