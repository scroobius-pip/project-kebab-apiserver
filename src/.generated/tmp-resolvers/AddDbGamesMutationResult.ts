// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { AddDbGamesMutationResultResolvers } from "../graphqlgen";

export const AddDbGamesMutationResult: AddDbGamesMutationResultResolvers.Type = {
  ...AddDbGamesMutationResultResolvers.defaultResolvers,

  error: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  }
};
