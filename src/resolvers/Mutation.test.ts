import { expect } from 'chai'
import Resolvers from '.'

describe('addGames()', () => {
  it('Should throw an Error if input.games is  empty ')
  it('Should throw an error if not authenticated')
})

describe('removeGames()', () => {
  it('Should throw an error if input.gameIds is empty ')
  it('Should throw an error if not authenticated')
})

describe('updateGames', () => {
  it('Should throw an error if input.games is empty')
  it('Should throw an error if not authenticated')
})

describe('createOffer()', () => {
  it('if input.myGames or input.otherGames is empty ')
  it('it should throw an error if otherId is empty')
  it('should throw an error if not authenticated')
})

describe('updateUserInfo()', () => {
  it('Should throw an error if not authenticated')
  it('Should throw an error if input.info is an empty object')
})

describe('updateOffer()', () => {
  it('Should throw an error if not authenticated')
  it('Should throw an error if offerId is empty string')
})
