import { QueryResolvers } from 'src/.generated/graphqlgen'
import OfferModel from '../models/Offer'
import UserModel from '../models/User'
import { UserInputError, AuthenticationError } from 'apollo-server'
import isEmpty from 'src/utils/isEmpty'
import GameModel from '../models/Game';
import MatchModel from '../models/Match';
import 'apollo-cache-control'
import { CacheScope } from 'apollo-cache-control';

export const Query: QueryResolvers.Type = {
  matches: (parent, { input }, context) => {
    if (!context.me) throw new AuthenticationError('No token passed')
    return MatchModel.getMatches(input, context)
  },
  offers: (parent, { input }, context) => {
    if (!context.me) throw new AuthenticationError('No token passed')
    return OfferModel.getOffers(input, context)
  },
  me: (parent, args, context) => {
    if (!context.me) throw new AuthenticationError('No token passed')
    return UserModel.getMe(context)
  },
  user: (parent, { input }, context) => UserModel.getUser(input, context),
  count: (_, __, context, info) => {
    info.cacheControl.setCacheHint({ maxAge: 60 * 1000, scope: CacheScope.Public })
    return UserModel.getCount(context)
  },
  searchGames: (parent, { input: { searchText, limit } }, context, info) => {
    info.cacheControl.setCacheHint({ maxAge: 31536000, scope: CacheScope.Public })
    if (!searchText.length) {
      return { result: [] }
    }
    return GameModel.searchGames({ searchText, limit }, context)
  }
}
