import { MutationResolvers } from 'src/.generated/graphqlgen'
import GameModel from '../models/Game'
import OfferModel from '../models/Offer'
import UserModel from '../models/User'
import updateOffer from '../models/Offer/updateOffer'
import { UserInputError, AuthenticationError } from 'apollo-server'
import isEmpty from '../utils/isEmpty'
const { addUserGames, removeUserGames, updateUserGames } = GameModel
const { createOffer } = OfferModel
const { updateUserInfo } = UserModel

export const Mutation: MutationResolvers.Type = {
  addUserGames: (parent, { input }, context) => {
    if (!context.me) throw new AuthenticationError('No token passed')
    if (!input.games.length) {
      return { result: true }
    }

    return addUserGames(input, context)
  },
  removeUserGames: (parent, { input }, context) => {
    if (!context.me) throw new AuthenticationError('No token passed')
    if (!input.games.length) {
      return { result: true }
    }

    return removeUserGames(input, context)
  },
  updateUserGames: (parent, { input }, context) => {
    if (!context.me) throw new AuthenticationError('No token passed')
    if (!input.games.length) {
      return { result: true }
    }

    return updateUserGames(input, context)
  },
  createOffer: (parent, { input }, context) => {
    if (!context.me) throw new AuthenticationError('No token passed')
    if (!input.offer.myGames.length || !input.offer.otherGames.length) {
      throw new UserInputError('myGames or OtherGames array is empty')
    }
    if (!input.offer.otherId.length) {
      throw new UserInputError('otherId is empty')
    }

    return createOffer(input, context)
  },
  updateUserInfo: (parent, { input }, context) => {
    if (!context.me) throw new AuthenticationError('No token passed')

    if (context.me.role !== 'ADMIN' && context.me.role !== 'INTERNAL') {
      delete input.info.isPro
      delete input.info.email
      delete input.info.isBanned
      delete input.info.user_subscriptionCancelUrl
      delete input.info.user_subscriptionUpdateUrl
    }

    if (isEmpty(input.info)) {
      throw new UserInputError('info is empty')
    }
    return updateUserInfo(input, context)
  },
  updateOffer: (parent, { input }, context) => {
    if (!context.me) throw new AuthenticationError('No token passed')
    if (!input.offerId.length) {
      throw new UserInputError('offerId is empty')
    }
    return updateOffer(input, context)
  },
  addDbGames: (parent, { input }, context) => {
    if (!context.me) throw new AuthenticationError('No token passed')
    const isInternal = context.me.role === 'INTERNAL'

    if (!isInternal) {
      throw new AuthenticationError('Invalid Role')
    }
    return GameModel.addDbGames(input, context)
  }
}
