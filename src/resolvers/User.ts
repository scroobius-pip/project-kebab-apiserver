import { UserResolvers } from 'src/.generated/graphqlgen'
import UserModel from '../models/User'
const { getUserGames } = UserModel

export const User: UserResolvers.Type = {
  id: (parent) => parent.id,
  info: (parent, args, ctx) => {


    const isOwner = ctx.me && ctx.me.id === parent.id
    const isAdmin = ctx.me && ctx.me.role === 'ADMIN'
    const isInternal = ctx.me && ctx.me.role === 'INTERNAL'
    return {
      ...parent.info,
      // email: (isOwner || isAdmin || isInternal) ? parent.info.email : '',

    }
  },
  wantedGames: (parent, args, ctx) => {
    return getUserGames(parent.id, 'want', ctx)
  },
  hasGames: (parent, args, ctx) => {
    return getUserGames(parent.id, 'has', ctx)
  },
}
