import { expect } from 'chai'
import Resolvers from '.'

describe('matches()', () => {
  it('Should throw an Error if input.has or input.wants is empty ')
  it('Should throw an error if not authenticated')
})

describe('offers()', () => {
  it('Should throw an error if not authenticated')
})

describe('me()', () => {
  it('Should throw an error if not authenticated')
})

describe('user()', () => {
  it('Should throw an error if userId is empty')
})
