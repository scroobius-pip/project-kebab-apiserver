import {
  ApolloServer,
  gql,
  AuthenticationError,
  ForbiddenError,
} from "apollo-server-micro";
import jwt from "jsonwebtoken";
import schema from "./schema/schemaString";
import resolvers from "./resolvers";
import { Context, DecodedToken, ContextUser } from "./types/types";
import DgraphConnector from "./connectors/DgraphConnector";
// tslint:disable-next-line: no-var-requires
import { values, isEmpty } from "lodash";
const microCors = require("micro-cors");
const cors = microCors({
  allowHeaders: [
    "X-Requested-With",
    "Access-Control-Allow-Origin",
    "X-HTTP-Method-Override",
    "Content-Type",
    "Authorization",
    "Accept",
    "token",
  ],
});

import { User } from "./types/models";
import { Connector } from "./connectors";

const JWT_TOKEN = process.env.JWT_TOKEN || "test";
const DGRAPH_ADDRESS = process.env.DGRAPH_ADDRESS || "localhost:9080";
const gqlSchema = gql(schema);

const verifyToken = (token: string, secret: string): DecodedToken => {
  try {
    const decodedToken = jwt.verify(token, secret) as object;
    const isValid = "email" in decodedToken && "role" in decodedToken;
    if (!isValid) throw new Error("Token does not contain some fields");
    return decodedToken as DecodedToken;
  } catch (error) {
    console.log(error.message);
    throw new AuthenticationError("Auth Token Invalid");
  }
};

const getOrCreateUser = async (
  { email, userName, profileImage = "", role }: DecodedToken,
  connector: Connector,
) => {
  const user = await connector.getUser.withUserEmail(email);
  if (user) return user;
  const uid = (await connector.createUser(email)).uid;
  const userNameExists = !!(await connector.getUser.withUserName(userName));
  await connector.updateUser({
    userName: `${userName}${userNameExists ? email.substr(0, 3) : ""}`,
    userImageUrl: profileImage,
  }, uid);
  return await connector.getUser.withUid(uid);
};

const parseUserToContextUser = (
  user: User,
  role: DecodedToken["role"],
): ContextUser => {
  return {
    email: user.info.email,
    id: user.id,
    isBanned: user.info.isBanned,
    isPro: user.info.isPro,
    role,
  };
};

const server = new ApolloServer({
  typeDefs: gqlSchema,
  resolvers: resolvers as any,
  playground: true,
  introspection: true,
  cacheControl: {
    calculateHttpHeaders: true,
    defaultMaxAge: 10,
  },
  engine: process.env.APOLLO_ENGINE_API_KEY &&
    { apiKey: process.env.APOLLO_ENGINE_API_KEY },
  context: async ({ req }: { req: any }): Promise<Context> => {
    let connector;
    try {
      connector = DgraphConnector(DGRAPH_ADDRESS);
    } catch (error) {
      throw new Error("Issue Connecting To Database");
    }

    const token = req.headers.token;
    if (!token) {
      return {
        me: null,
        connector,
      };
    }

    const tokenValue = verifyToken(token, JWT_TOKEN);
    const user = await getOrCreateUser(tokenValue, connector);

    if (user.info.isBanned) throw new ForbiddenError("User is Banned");

    return {
      me: parseUserToContextUser(user, tokenValue.role),
      connector,
    };
  },
});

// @ts-ignore
export default cors((req, res) => {
  if (req.method === "OPTIONS") {
    res.end();
    return;
  }
  return server.createHandler()(req, res);
});
