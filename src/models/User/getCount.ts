import { Context } from 'src/types/types';
import { UserModelType } from '.';

const getCount: UserModelType['getCount'] = async ({ connector }) => {
    return await connector.getUserCount()
}

export default getCount