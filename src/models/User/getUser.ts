import { UserModelType } from '.'

const getUser: UserModelType['getUser'] = async ({ by, value }, { connector }) => {
  if (by === 'username') {
    return await connector.getUser.withUserName(value)
  }
  return await connector.getUser.withUid(value)
}

export default getUser