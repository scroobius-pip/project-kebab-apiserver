import { UserModelType } from '.'

const getMe: UserModelType['getMe'] = (context) => {
  const { me, connector } = context

  return connector.getUser.withUserEmail(me.email)
}

export default getMe
