import { UserModelType } from '.'
import { Error } from 'src/types/models'

const updateUserInfo: UserModelType['updateUserInfo'] = async ({ info }, context) => {
  if (info.userName) {
    const user = await context.connector.getUser.withUserName(info.userName)
    if (user) return {
      error: {
        id: '',
        message: 'User Name Exists',
        type: 'AUTH_ERROR'
      } as Error
    }
  }

  if (info.email) {
    const user = await context.connector.getUser.withUserEmail(info.email)
    if (user) return {
      error: {
        id: '',
        message: 'Email Exists',
        type: 'AUTH_ERROR'
      } as Error
    }
  }

  if (info.setting_matchNotifications && !context.me.isPro) {
    return {
      error: {
        id: '',
        message: 'Upgrade To Premium',
        type: 'UPGRADE_MEMBERSHIP'
      } as Error
    }
  }
  try {
    await context.connector.updateUser(info, context.me.id)
    const userInfo = (await context.connector.getUser.withUid(context.me.id)).info
    return { result: userInfo }
  } catch (error) {
    console.error(error)
    throw error
  }
}
export default updateUserInfo
