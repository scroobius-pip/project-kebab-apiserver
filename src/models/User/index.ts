import getMe from './getMe'
import getUser from './getUser'
import getCount from './getCount'
import getUserGames from './getUserGames'
import updateUserInfo from './updateUserInfo'
import { User, UserGame, UpdateUserInfoMutationResult } from 'src/types/models'
import {
  MutationResolvers,
  UserGameDetailsStatus,
  QueryResolvers
} from 'src/.generated/graphqlgen'
import { Context } from 'src/types/types'

export interface UserModelType {
  getMe: (context: Context) => User | Promise<User>
  getUserGames: (
    userId: string,
    status: UserGameDetailsStatus,
    context: Context
  ) => UserGame[] | Promise<UserGame[]>
  updateUserInfo: (
    input: MutationResolvers.UpdateUserInfoMutationInput,
    context: Context
  ) => UpdateUserInfoMutationResult | Promise<UpdateUserInfoMutationResult>
  getUser: (input: QueryResolvers.UserQueryInput, context: Context) => User | Promise<User>
  getCount: (context: Context) => number | Promise<number>
}

const UserModel: UserModelType = {
  getMe,
  getUserGames,
  updateUserInfo,
  getUser,
  getCount
}

export default UserModel
