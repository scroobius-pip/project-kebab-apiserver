import { UserModelType } from '.'

const getUserGamesFromUserId: UserModelType['getUserGames'] = (
  userId,
  status,
  context
) => {
  return context.connector.getUserGames(userId, status)
}

export default getUserGamesFromUserId
