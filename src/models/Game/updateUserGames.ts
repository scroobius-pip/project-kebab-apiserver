import { GameModelType } from '.'

const updateGames: GameModelType['updateUserGames'] = async ({ games }, context) => {
  try {
    await context.connector.updateUserGames(games, context.me.id)
    return {
      result: true
    }
  } catch (error) {
    console.error(error)
    return {
      result: false
    }
  }
}

export default updateGames
