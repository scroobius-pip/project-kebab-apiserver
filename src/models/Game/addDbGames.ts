import { GameModelType } from '.'
import { ApolloError } from 'apollo-server'
import { AddDbGamesMutationResult } from 'src/types/models';

const addDbGames: GameModelType['addDbGames'] = async ({ games }, context) => {
    try {
        const gamesNotAdded = await context.connector.createDbGames(games)
        return {
            result: true,
            notAdded: gamesNotAdded
        }
    } catch (error) {
        console.error(error)

        return { result: false, notAdded: games, error: error.message }

    }
}

export default addDbGames