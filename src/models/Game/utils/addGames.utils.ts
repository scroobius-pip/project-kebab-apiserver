const isGameQuotaFull = (
  currentNoOfHasGames: number,
  hasGamesNo: number,
  currentNoOfWantedGames: number,
  wantsGamesNo: number,
  gameLimit: number
) =>
  currentNoOfHasGames + hasGamesNo > gameLimit ||
  currentNoOfWantedGames + wantsGamesNo > gameLimit

const isStatus = (
  game: { details: { status: 'has' | 'want' } },
  status: 'has' | 'want'
) => game.details.status === status

const isHasStatus = (game: { details: { status: 'has' | 'want' } }) =>
  isStatus(game, 'has')
const isWantStatus = (game: { details: { status: 'has' | 'want' } }) =>
  isStatus(game, 'want')

// return true if game quota full else false
const checkGameQuota = async (
  userId: string,
  games: any[],
  getNoOfUserGames: (_: string, __: 'has' | 'want') => Promise<number> | number
) => {
  const hasGamesNo = games.filter(isHasStatus).length
  const wantsGamesNo = games.filter(isWantStatus).length
  const currentNoOfHasGames = await getNoOfUserGames(userId, 'has')
  const currentNoOfWantedGames = await getNoOfUserGames(userId, 'want')
  return {
    currentNoOfHasGames,
    hasGamesNo,
    currentNoOfWantedGames,
    wantsGamesNo
  }
}

export { isGameQuotaFull, isHasStatus, isWantStatus, checkGameQuota }
