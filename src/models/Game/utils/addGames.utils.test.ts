import { expect } from 'chai'
import {
  checkGameQuota,
  isGameQuotaFull,
  isHasStatus,
  isWantStatus
} from './addGames.utils'

describe('isHasStatus()', () => {
  it('if game.details.status === has return true', () => {
    expect(isHasStatus({ details: { status: 'has' } })).to.equal(true)
  })

  it('if game.details.status === want return false', () => {
    expect(isHasStatus({ details: { status: 'want' } })).to.equal(false)
  })
})

describe('isWantStatus()', () => {
  it('if game.details.status === has return true', () => {
    expect(isWantStatus({ details: { status: 'want' } })).to.equal(true)
  })

  it('if game.details.status === want return false', () => {
    expect(isWantStatus({ details: { status: 'has' } })).to.equal(false)
  })
})

describe('checkGameQuota()', () => {
  const games = [
    { details: { status: 'want' } },
    { details: { status: 'has' } },
    { details: { status: 'want' } },
    { details: { status: 'has' } },
    { details: { status: 'want' } }
  ]

  const games2 = [
    { details: { status: 'want' } },
    { details: { status: 'want' } }
  ]

  const games3: any = []

  it('checkgameQuota should work well', async () => {
    const result = {
      currentNoOfHasGames: 2,
      currentNoOfWantedGames: 2,
      hasGamesNo: 2,
      wantsGamesNo: 3
    }

    const result2 = {
      currentNoOfHasGames: 2,
      currentNoOfWantedGames: 2,
      hasGamesNo: 0,
      wantsGamesNo: 2
    }

    const result3 = {
      currentNoOfHasGames: 2,
      currentNoOfWantedGames: 2,
      hasGamesNo: 0,
      wantsGamesNo: 0
    }

    const quota = await checkGameQuota('', games, (_: string, __: any) => 2)
    const quota2 = await checkGameQuota('', games2, (_: string, __: any) => 2)
    const quota3 = await checkGameQuota('', games3, (_: string, __: any) => 2)
    expect(quota).to.deep.equals(result)
    expect(quota2).to.deep.equals(result2)
    expect(quota3).to.deep.equals(result3)
  })
})

describe('isGameQuotaFull()', () => {
  it('isGameQuotaFull should work well', async () => {
    const result = await isGameQuotaFull(2, 2, 2, 2, 2)
    const result2 = await isGameQuotaFull(2, 2, 2, 2, 20)
    const result3 = await isGameQuotaFull(0, 0, 0, 0, 2)

    expect(result).to.equal(true)
    expect(result2).to.equal(false)
    expect(result3).to.equal(false)
  })
})
