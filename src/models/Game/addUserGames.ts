import { GameModelType } from '.'
import { ApolloError, AuthenticationError } from 'apollo-server'
import { checkGameQuota, isGameQuotaFull } from './utils/addGames.utils'
import { AddGamesMutationResult, Error } from 'src/types/models';

const QUOTA_LIMIT = 100

const addUserGames: GameModelType['addUserGames'] = async ({ games }, context) => {
  try {
    const {
      id,
      isPro
    } = context.me

    if (isPro) {
      await context.connector.addUserGames(games, id)
      return {
        result: true
      }
    } else {
      const gameQuota = await checkGameQuota(
        id,
        games,
        context.connector.getNoOfUserGames
      )

      const {
        currentNoOfHasGames,
        currentNoOfWantedGames,
        hasGamesNo,
        wantsGamesNo
      } = gameQuota

      const isQuotaFull = isGameQuotaFull(
        currentNoOfHasGames,
        hasGamesNo,
        currentNoOfWantedGames,
        wantsGamesNo,
        QUOTA_LIMIT
      )

      if (isQuotaFull) {
        return {
          result: false,
          error: { id: '', message: 'Upgrade To Premium', type: 'UPGRADE_MEMBERSHIP' } as Error
        }

      }

      await context.connector.addUserGames(games, id)
      return {
        result: true,
      }

    }
  } catch (error) {
    console.error(error)
    return { result: false }
  }
}

export default addUserGames
