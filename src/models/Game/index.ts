import addUserGames from './addUserGames'
import removeUserGames from './removeUserGames'
import updateUserGames from './updateUserGames'
import addDbGames from './addDbGames'
import {
  AddGamesMutationResult,
  RemoveGamesMutationResult,
  UpdateGamesMutationResult,
  AddDbGamesMutationResult,
  SearchGamesQueryResult
} from 'src/types/models'
import { MutationResolvers, QueryResolvers } from 'src/.generated/graphqlgen'
import { Context } from 'src/types/types'
import searchGames from './searchGames';

export interface GameModelType {
  addUserGames: (
    input: MutationResolvers.AddGamesMutationInput,
    context: Context
  ) => AddGamesMutationResult | Promise<AddGamesMutationResult>
  removeUserGames: (
    input: MutationResolvers.RemoveGamesMutationInput,
    context: Context
  ) => RemoveGamesMutationResult | Promise<RemoveGamesMutationResult>
  updateUserGames: (
    input: MutationResolvers.UpdateGamesMutationInput,
    context: Context
  ) => UpdateGamesMutationResult | Promise<UpdateGamesMutationResult>
  addDbGames: (
    input: MutationResolvers.AddDbGamesMutationInput,
    context: Context,
  ) => AddDbGamesMutationResult | Promise<AddDbGamesMutationResult>
  searchGames: (
    input: QueryResolvers.SearchGamesQueryInput,
    context: Context) => SearchGamesQueryResult | Promise<SearchGamesQueryResult>
}

const GameModel: GameModelType = {
  addUserGames,
  removeUserGames,
  updateUserGames,
  addDbGames,
  searchGames
}

export default GameModel
