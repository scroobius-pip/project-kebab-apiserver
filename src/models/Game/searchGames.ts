import { GameModelType } from '.';

const searchGames: GameModelType['searchGames'] = async ({ searchText, limit }, context) => {
    const games = await context.connector.searchDbGames(searchText, limit)

    return {
        result: games
    }
}

export default searchGames