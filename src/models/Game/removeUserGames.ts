import { GameModelType } from '.'

const removeUserGames: GameModelType['removeUserGames'] = async (input, context) => {
  try {
    await context.connector.removeUserGames(input.games, context.me.id)
    return {
      result: true
    }
  } catch (error) {
    console.error(error)
    return {
      result: false
    }
  }

}

export default removeUserGames
