import { MatchModelType } from '.'
import { MatchQueryResult, Error, Match, ErrorType } from 'src/types/models';
import { ApolloError } from 'apollo-server'

const obscureMatchUser = (match: Match): Match => {
  return {
    ...match,
    userName: match.userName.length > 3 ? match.userName.slice(0, 3) + '*'.repeat(match.userName.length - 3) : '***'
  }
}

const getMatches: MatchModelType['getMatches'] = async (args, context) => {
  const { byLocation, byMatchRate } = context.connector.getMatches
  const matchFunction = args.sortBy === 'location' ? byLocation : byMatchRate

  try {
    const { info: { location: { state, country } } } = await context.connector.getUser.withUid(context.me.id)
    if (args.sortBy === 'location' && !state.length) {
      return {
        result: [],
        error: {
          id: '',
          message: 'Enable Location in Settings',
          type: 'LOCATION_NOT_SET' as ErrorType
        }
      }
    }
    const matches = await matchFunction(context.me.id)
    return {
      result: context.me.isPro ? matches : matches.map((match, index) => {
        return index < 2 ? match : obscureMatchUser(match)
      })
    }
  } catch (error) {
    console.log(error)
    return {
      result: [],
      error: {
        id: '',
        message: 'Internal error please try again later',
        type: 'INTERNAL_ERROR' as ErrorType
      }
    }
  }
}

export default getMatches
