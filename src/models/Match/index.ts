import getMatches from './getMatches'
import { MatchQueryResult } from 'src/types/models'
import { QueryResolvers } from 'src/.generated/graphqlgen'
import { Context } from 'src/types/types'

export interface MatchModelType {
  getMatches: (
    input: QueryResolvers.MatchQueryInput,
    context: Context
  ) => MatchQueryResult | Promise<MatchQueryResult>
}

const MatchModel: MatchModelType = {
  getMatches
}

export default MatchModel
