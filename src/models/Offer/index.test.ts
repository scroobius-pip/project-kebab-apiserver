describe('updateOffer', () => {
  it('"Accept & Decline" can only affect pending offers')
  it('Receiving User can only accept offer')
  it('"Accept" changes offer-status from pending to ongoing')

  it('"Complete" affects only pending offers')
  it('Allow only other user (receiving user) to accept an offer')
})

describe('getOffers', () => {
  it('')
})

describe('createOffer', () => {
  it('Throw Authorization Error if !isPro && number of offers > 10')
})
