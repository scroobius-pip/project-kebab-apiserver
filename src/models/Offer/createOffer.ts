import { OfferModelType } from '.'
import { Error } from 'src/types/models'

const createOffer: OfferModelType['createOffer'] = async (
  { offer },
  context
) => {
  const { connector, me } = context
  const noOfOffers = (await connector.getUserOffers('pending', me.id, { limit: 0, offset: 0 })).pageInfo.noOfItems
  if (!context.me.isPro && noOfOffers > 10) {
    return {

      error: { id: '', message: 'Upgrade To Premium', type: 'UPGRADE_MEMBERSHIP' } as Error
    }
  } else {
    const offerId = (await connector.createOffer(offer, me.id)).uid;
    return {
      result: await connector.getOffer(offerId, me.id)
    }
  }

}
export default createOffer
