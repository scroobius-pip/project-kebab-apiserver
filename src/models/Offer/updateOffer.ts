import { OfferModelType } from '.'
import { ForbiddenError } from 'apollo-server'
import OfferStateMachine from './_utils/offerStateMachine'
const updateOffer: OfferModelType['updateOffer'] = async ({ offerId, type }, { connector, me }) => {
  const isParticipatingInOffer = await connector.participatesInOffer(offerId, me.id)

  if (!isParticipatingInOffer) {
    throw new ForbiddenError('Not Participating In Offer')
  }

  const Offer = await connector.getOffer(offerId, me.id)
  const userType = Offer.senderId === me.id ? 'sender' : 'receiver'
  const nextOfferState = OfferStateMachine(userType, type, {
    offer: Offer.status,
    receiver: Offer.receiverStatus,
    sender: Offer.senderStatus
  })

  if (!nextOfferState) throw new Error('Invalid Operation')

  await connector.setUserOfferStatus(Offer.receiverId, 'receiver', offerId, nextOfferState.receiver) // set receiver status
  await connector.setUserOfferStatus(Offer.senderId, 'sender', offerId, nextOfferState.sender) //set sender status
  await connector.updateOffer(offerId, nextOfferState.offer) //set offer status
  if (nextOfferState.offer === 'completed') {
    // await connector.updateUser({},)
  }
  return {
    result: (await connector.getOffer(offerId, me.id))
  }
}
export default updateOffer
