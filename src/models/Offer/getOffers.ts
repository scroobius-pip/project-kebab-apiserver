import { OfferModelType } from '.'

const getOffers: OfferModelType['getOffers'] = (
  { status, pagination },
  context
) => {
  return context.connector.getUserOffers(status, context.me.id, pagination)
}
export default getOffers
