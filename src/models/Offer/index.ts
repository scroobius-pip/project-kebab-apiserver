import createOffer from './createOffer'
import updateOffer from './updateOffer'
import getOffers from './getOffers'
import {
  CreateOfferMutationResult,
  OfferQueryResult,
  UpdateOfferMutationResult
} from 'src/types/models'
import { MutationResolvers, QueryResolvers } from 'src/.generated/graphqlgen'
import { Context } from 'src/types/types'

export interface OfferModelType {
  createOffer: (
    input: MutationResolvers.CreateOfferMutationInput,
    context: Context
  ) => CreateOfferMutationResult | Promise<CreateOfferMutationResult>

  getOffers: (
    input: QueryResolvers.OfferQueryInput,
    context: Context
  ) => OfferQueryResult | Promise<OfferQueryResult>

  updateOffer: (
    input: MutationResolvers.UpdateOfferMutationInput,
    context: Context
  ) => UpdateOfferMutationResult | Promise<UpdateOfferMutationResult>
}

const OfferModel: OfferModelType = {
  createOffer,
  getOffers,
  updateOffer
}

export default OfferModel
