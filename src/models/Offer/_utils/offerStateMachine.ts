import { UpdateOfferMutationInputType, OfferStatus } from 'src/.generated/graphqlgen';
interface NextState {
    'sender': OfferStatus,
    'receiver': OfferStatus,
    'offer': OfferStatus
}

interface StateTransition {
    'ongoingpendingpending': {
        'acceptreceiver': NextState,
        'declinereceiver': NextState
    },
    'ongoingongoingongoing': {
        'cancelsender': NextState,
        'cancelreceiver': NextState,
        'completereceiver': NextState,
        'completesender': NextState,
    },
    'cancelledongoingongoing': {
        'cancelreceiver': NextState,

    },
    'ongoingcancelledongoing': {
        'cancelsender': NextState
    },
    'ongoingcompletedongoing': {
        'completesender': NextState,
    },
    'completedongoingongoing': {
        'completereceiver': NextState
    },
    'declineddeclineddeclined': {}, // terminal state
    'completedcompletedccompleted': {}, // terminal state
    'cancelledcancelledcancelled': {}, // terminal state
    [key: string]: {}
}

const StateMachine: StateTransition = {
    'ongoingpendingpending': {
        'acceptreceiver': {
            sender: 'ongoing',
            offer: 'ongoing',
            receiver: 'ongoing'
        },
        'declinereceiver': {
            sender: 'declined',
            receiver: 'declined',
            offer: 'declined'
        }
    },
    'ongoingongoingongoing': {
        'cancelsender': {
            sender: 'cancelled',
            receiver: 'ongoing',
            offer: 'ongoing'
        },
        'cancelreceiver': {
            sender: 'ongoing',
            receiver: 'cancelled',
            offer: 'ongoing'
        },
        'completereceiver': {
            sender: 'ongoing',
            receiver: 'completed',
            offer: 'ongoing'
        },
        'completesender': {
            sender: 'completed',
            receiver: 'ongoing',
            offer: 'ongoing'
        },
    },
    'cancelledongoingongoing': {
        'cancelreceiver': {
            sender: 'cancelled',
            receiver: 'cancelled',
            offer: 'cancelled'
        },

    },
    'ongoingcancelledongoing': {
        'cancelsender': {
            sender: 'cancelled',
            receiver: 'cancelled',
            offer: 'cancelled'
        }
    },
    'ongoingcompletedongoing': {
        'completesender': {
            sender: 'completed',
            receiver: 'completed',
            offer: 'completed'
        },

    },
    'completedongoingongoing': {
        'completereceiver': {
            sender: 'completed',
            receiver: 'completed',
            offer: 'completed'
        }
    },
    'declineddeclineddeclined': {}, // terminal state
    'completedcompletedccompleted': {}, // terminal state
    'cancelledcancelledcancelled': {}, // terminal state

}

const Transition = (userType: 'sender' | 'receiver', operation: UpdateOfferMutationInputType, currentState: {
    sender: OfferStatus,
    receiver: OfferStatus,
    offer: OfferStatus
}): NextState | null => {
    type state = keyof StateTransition | string
    const input = operation + userType
    const state: state = currentState.sender + currentState.receiver + currentState.offer

    if (!(state in StateMachine)) return null
    if (!(input in StateMachine[state])) return null
    // @ts-ignore
    return StateMachine[state][input]
}

export default Transition