import { expect } from 'chai'
import OfferStateMachine from './offerStateMachine'
// import mergeOperation, { Operation } from './mergeOperations'

describe('OfferStateMachine', () => {
    it('should return null if input does not exist in state', () => {
        const result = OfferStateMachine('sender', 'accept', {
            offer: 'pending',
            receiver: 'pending',
            sender: 'ongoing'
        })
        expect(result).to.be.null

        const result2 = OfferStateMachine('receiver', 'decline', {
            offer: 'declined',
            receiver: 'declined',
            sender: 'declined'
        })
        expect(result2).to.be.null
    })

    it('should return null if state does not exist', () => {
        const result = OfferStateMachine('receiver', 'complete', {
            offer: 'ongoing',
            receiver: 'completed',
            sender: 'completed'
        })
        expect(result).to.be.null
    })

    it('should return correct states', () => {
        const r1 = OfferStateMachine('receiver', 'accept', { offer: 'pending', sender: 'ongoing', receiver: 'pending' })
        const r2 = OfferStateMachine('receiver', 'complete', { ...r1 })
        const r3 = OfferStateMachine('sender', 'complete', { ...r2 })
        expect(r3).to.deep.equal({
            receiver: 'completed',
            sender: 'completed',
            offer: 'completed'
        })

        const q1 = OfferStateMachine('receiver', 'decline', { offer: 'pending', sender: 'ongoing', receiver: 'pending' })
        expect(q1).to.deep.equal({
            receiver: 'declined',
            sender: 'declined',
            offer: 'declined'
        })
    })
})

// describe('mergeOperation', () => {
//     it('should remove operations with delete and add', () => {
//         const operation: Operation = {

//             add: { name: 'simdi', description: '' },
//             delete: true,
//             update: { name: 'simdi', description: 'hello' }
//         }



//         expect(mergeOperation(operation)).to.be.null
//     })

//     it('should merge add && update', () => {
//         const operationGroups: Operation = {

//             add: { name: 'simdi', description: 'a' },

//             update: { name: 'simdi', description: 'hello' }
//         }



//         expect(mergeOperation(operationGroups)).to.deep.equal(
//             {

//                 add: { name: 'simdi', description: 'hello' },

//             }

//         )
//     })

//     it('should ignore update, when delete is true', () => {
//         const operationGroups: Operation = {

//             delete: true,
//             update: { name: 'simdi', description: 'hello' }
//         }



//         expect(mergeOperation(operationGroups)).to.deep.equal(
//             {


//                 delete: true,

//             }

//         )
//     })


//     it('should add when nothing else to do', () => {
//         const operationGroups: Operation = {
//             add: { name: 'simdi', description: '' },

//         }


//         expect(mergeOperation(operationGroups)).to.deep.equal(
//             {

//                 add: { name: 'simdi', description: '' },

//             }
//         )
//     })


//     it('should delete when nothing else to do', () => {
//         const operationGroups: Operation = {
//             delete: true,
//         }

//         expect(mergeOperation(operationGroups)).to.deep.equal(
//             {

//                 delete: true,

//             }

//         )
//     })

//     it('should update when nothing else to do', () => {
//         const operationGroups: Operation = {

//             update: { name: 'simdi', description: 'hello' }
//         }
//         expect(mergeOperation(operationGroups)).to.deep.equal({
//             update: { name: 'simdi', description: 'hello' }
//         })
//     })
// })
