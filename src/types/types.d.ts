import { User } from './models'
import { Connector } from 'src/connectors'

export interface DecodedToken {
  email: string
  role: 'ADMIN' | 'INTERNAL' | 'USER'
  userName?: string
  profileImage?: string
}


interface ContextUser {
  id: string
  email: string
  isPro: boolean
  isBanned: boolean
  role: DecodedToken['role']
}

export interface Context {
  me?: ContextUser
  connector: Connector
}

