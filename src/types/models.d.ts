
export type OfferStatus = 'pending' | 'ongoing' | 'completed' | 'cancelled'
export type ErrorType = 'UPGRADE_MEMBERSHIP' | 'AUTH_ERROR' | 'LOCATION_NOT_SET' | 'INTERNAL_ERROR'
export interface MatchQueryResult {
  result?: Match[]
  error?: Error
}
export interface Match {
  id: string
  wantedGameNames: string[]
  hasGameNames: string[]
  userImageUrl: string
  userName: string
  matchRate?: number
  country?: string
  state?: string
}

export interface User {
  id: string
  info: UserInfo
}

export interface UserInfo {
  email?: string
  description?: string
  isPro?: boolean
  epochTimeCreated?: string
  location?: UserInfoLocation
  rating?: UserInfoRating
  noOfSuccessfulExchanges?: number
  userImageUrl?: string
  userName?: string
  isBanned?: boolean
  setting_matchNotifications?: boolean
  user_subscriptionCancelUrl?: string
  user_subscriptionUpdateUrl?: string
}

export interface UserInfoDate {
  month: string
  year: number
}
export interface UserInfoLocation {
  country: string
  state: string
}
export interface UserInfoRating {
  negative: number
  positive: number
}
export interface UserGame {
  id: string
  details: UserGameDetails
  game: Game
}
export interface UserGameDetails {
  status?: 'has' | 'want'
  tradeType: 'swap' | 'sale'
  description: string
}

export interface UserGameDetailsInput { }
export interface Game {
  name: string
  consoleType: string | null
  id: string
  imageUrl: string | null
}
export interface PageInfo {
  noOfItems: number
}
export interface Error {
  id?: string
  message?: string
  type?: ErrorType
}
export interface OfferQueryResult {
  result: Offer[]
  pageInfo: PageInfo
}
export interface Offer {
  offerId: string
  senderGames: UserGame[]
  receiverGames: UserGame[]
  receiverId: string
  senderId: string
  epochTimeCreated: string
  status: OfferStatus
  receiverStatus: OfferStatus,
  senderStatus: OfferStatus
}

export interface AddGamesMutationResult {
  result?: boolean
  error?: Error
}
export interface RemoveGamesMutationResult {
  result: boolean
}
export interface UpdateGamesMutationResult {
  result: boolean
}

export interface CreateOfferMutationResult {
  result?: Offer
  error?: Error
}
export interface UpdateUserInfoMutationResult {
  result?: UserInfo
  error?: Error
}
export interface UpdateOfferMutationResult {
  result: Offer
}

export interface AddDbGamesMutationResult {
  result?: boolean
  notAdded?: AddDbGamesMutationResultNotAdded[]
}

export interface SearchGamesQueryResult {
  result: Game[]
}

export interface AddDbGamesMutationResultNotAdded {
  name: string
  consoleType: string
  imageUrl: string
  popularity: number
}
