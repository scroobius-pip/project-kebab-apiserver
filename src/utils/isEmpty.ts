import { isEmpty } from 'lodash'

export default (obj: object): boolean => {
  return isEmpty(obj)
}