
import dgraph = require('dgraph-js')
import { MutationResolvers } from 'src/.generated/graphqlgen';
import gameExists from './_utils/gameExists';
import generateGameDbUuid from './_utils/generateGameDbUuid';

if (typeof (Symbol as any).asyncIterator === 'undefined') {
    (Symbol as any).asyncIterator = Symbol.asyncIterator || Symbol('asyncIterator');
}

interface GameDb {
    game: ''
    'game.name': string
    'game.coverUrl': string
    'game.platform': string
    'game.popularity': number
    'game.uuid': string
}

const addGameToDb = async (game: GameDb[], transaction: dgraph.Txn): Promise<void> => {

    const mutation = new dgraph.Mutation()
    mutation.setSetJson(game)
    await transaction.mutate(mutation)
}

const parseGameToDb = (game: MutationResolvers.AddDbGamesMutationInput['games'][0]): GameDb => {
    return {
        'game': '',
        'game.coverUrl': game.imageUrl,
        'game.name': game.name,
        'game.platform': game.consoleType,
        'game.popularity': game.popularity,
        'game.uuid': generateGameDbUuid(game.name, game.consoleType)
    }
}

export default async (
    games: MutationResolvers.AddDbGamesMutationInput['games'],
    client: dgraph.DgraphClient
) => {
    const transaction = client.newTxn()
    const gamesAlreadyAdded = []
    try {
        const gamesDb: GameDb[] = []

        for await (const game of games) {
            const r = await gameExists(game.name, game.consoleType, transaction)
            if (!r) {
                const gameDb = parseGameToDb(game)
                gamesDb.push(gameDb)
            } else {
                gamesAlreadyAdded.push(game)
            }
        }

        await addGameToDb(gamesDb, transaction)
        await transaction.commit()
        return gamesAlreadyAdded
    } catch (error) {
        console.error(error)
        throw error
    } finally {
        transaction.discard()

    }
}