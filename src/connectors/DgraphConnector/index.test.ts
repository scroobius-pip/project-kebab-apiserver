import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';
import { MutationResolvers } from 'src/.generated/graphqlgen';
import { User, UserGameDetails, UserGame, Match } from 'src/types/models';
import dgraphInit from './';
import GameExists from './_utils/gameExists';
import getAllUserGames from './_utils/getAllUserGames';

const expect = chai.expect
chai.use(chaiAsPromised)
import dgraph = require('dgraph-js');

const connector = dgraphInit()

const UID = '0xc1'
const USER_NAME = 'sim04ful'
const USER_EMAIL = 'sim04ful@gmail.com'
const UID_OTHER = '0xc3'
const USER_NAME_OTHER = 'amomi'
const USER_EMAIL_NOT_CREATED = 'somsom@gmail.com'
const UID_NOT_CREATED = '0x99999'
const USER_NAME_NOT_CREATED = 'somsom'
const NOT_PARTICIPATES_OFFER_ID = '0xd4'
const PARTICIPATES_OFFER_ID = '0xb2'
const GAMES_TO_ADD: MutationResolvers.AddGamesInput[] = [{
  details: {
    description: 'first game added',
    tradeType: 'swap',
    status: 'want'
  },
  gameId: '0xa3'
}, {
  details: {
    description: 'second game added',
    tradeType: 'sale',
    status: 'want'
  },
  gameId: '0xa5'
},
{
  details: {
    description: 'second custom game added',
    tradeType: 'sale',
    status: 'want'
  },
  gameId: '',
  customItemDetails: {
    consoleType: 'Custom Item',
    name: 'Custom Item 2'
  }
}

]
const GAMES_TO_REMOVE: MutationResolvers.RemoveGamesInput[] = [
  { id: '0xab1' },
  { id: '0xab4' }
]
const GAMES_TO_UPDATE: MutationResolvers.UpdateGamesInput[] = [
  { details: { description: 'game1updatedescription', status: 'want', tradeType: 'swap' }, id: '0xab3' },
  { details: { description: 'game2updatedescription', status: 'has', tradeType: 'sale' }, id: '0xab2' },
  { details: { description: 'customgame1updateddescription', status: 'has', tradeType: 'swap' }, id: '0xab4' }
]
const USER_WANTS: string[] = []
const USER_HAS: string[] = []
const OFFER_MY_GAMES: MutationResolvers.UserGameInput[] = [
  {
    details: { description: '', tradeType: 'swap', },
    gameId: '0xa4',

  }
]
const OFFER_OTHER_GAMES: MutationResolvers.UserGameInput[] = [
  {
    details: { description: '', tradeType: 'swap' },
    gameId: '0xa3'
  }
]

const OFFER_MY_GAMES_EDITED: MutationResolvers.UserGameInput[] = [{
  details: { description: '', tradeType: 'swap' },
  gameId: '0xa2'
},
{
  details: { description: '', tradeType: 'swap' },
  gameId: '0xa4'
}
]

const OFFER_OTHER_GAMES_EDITED: MutationResolvers.UserGameInput[] = [
  {
    details: { description: '', tradeType: 'swap' },
    gameId: '0xa3'
  }
]

const isDescending = (arr: number[]) => {
  return arr.every((x, i) => {
    return i === 0 || x <= arr[i - 1]
  })
}

// describe('Should throw correct errors when database fails')

describe(`connector.seedDb()`, () => {
  it('Should not throw', () => {
    expect(connector.seedDb()).to.not.be.rejected
  })

})

describe('Connector Behaviour Tests', () => {

  beforeEach(async () => {
    await connector.clearDb()
    await connector.seedDb()
  })

  after(async () => {
    await connector.clearDb()
  })

  describe('connector.addGames()', () => {

    afterEach(async () => {
      await connector.clearDb()
    })

    it('Should not be the same before and after addition', async () => {
      const initialUserGames = await getAllUserGames(UID, connector)
      await connector.addUserGames(GAMES_TO_ADD, UID)
      const currentUserGames = await getAllUserGames(UID, connector)

      expect(initialUserGames).to.not.deep.equal(currentUserGames)
    })

    it('Should add games to correct user', async () => {
      await connector.addUserGames(GAMES_TO_ADD, UID)

      const userGames = await getAllUserGames(UID, connector)
      const gameToAddDetails = GAMES_TO_ADD.map(game => game.details.description)
      const userGameDetails = userGames.map(game => game.details.description)

      const gameToAddName = GAMES_TO_ADD.map(game => game.customItemDetails ? game.customItemDetails.name : null)
        .filter(game => !!game)
      const userGameName = userGames.map(game => game.game.name)

      expect(userGameName).to.include.members(gameToAddName)
      expect(userGameDetails).to.include.members(gameToAddDetails)

    })

    it('Should return all user games including those added', async () => {
      await connector.addUserGames(GAMES_TO_ADD, UID)
      const userGames = await getAllUserGames(UID, connector)
      const userGameDescriptions = userGames.map(game => game.details.description)
      const gameToAddDescriptions = GAMES_TO_ADD.map((x) => x.details.description)

      expect(userGameDescriptions).to.include.members(gameToAddDescriptions)

    })
  })

  describe('removeGames()', () => {

    beforeEach(async () => {
      await connector.clearDb()
      await connector.seedDb()
    })

    it('Should not be the same before and after removal', async () => {
      // const noofGameToRemove = 

      const initialUserGames = await getAllUserGames(UID, connector)
      await connector.removeUserGames(GAMES_TO_REMOVE, UID)
      const currentUserGames = await getAllUserGames(UID, connector)

      expect(initialUserGames).to.not.deep.equal(currentUserGames)
      expect(initialUserGames.length).to.be.greaterThan(currentUserGames.length)
      expect(initialUserGames.length - GAMES_TO_REMOVE.length).to.be.equal(currentUserGames.length)
    })

    it('Should remove games from correct user', async () => {
      await connector.removeUserGames(GAMES_TO_REMOVE, UID)
      const userGameIds = (await getAllUserGames(UID, connector)).map(game => game.id)
      const gamesToRemoveIds = GAMES_TO_REMOVE.map(game => game.id)

      expect(userGameIds).to.not.include.members(gamesToRemoveIds)
    })
  })

  describe('updateGames()', () => {
    beforeEach(async () => {
      await connector.clearDb()
      await connector.seedDb()
    })

    it('Should update games from correct user', async () => {
      await connector.updateUserGames(GAMES_TO_UPDATE, UID)
      const userGamesAfterUpdate = (await getAllUserGames(UID, connector)).map(game => game.details)
      const gameToUpdateDetails: UserGameDetails[] = GAMES_TO_UPDATE.map(game => game.details)

      expect(userGamesAfterUpdate).to.deep.include.members(gameToUpdateDetails)
    })

    it('Number of games should not change', async () => {
      const userGamesBeforeUpdate = (await getAllUserGames(UID, connector)).map(games => games.details)
      await connector.updateUserGames(GAMES_TO_UPDATE, UID)
      const userGamesAfterUpdate = (await getAllUserGames(UID, connector)).map(game => game.details)

      expect(userGamesBeforeUpdate.length).to.be.equal(userGamesAfterUpdate.length)
    })

    // it('Should update custom')
  })

  describe('getMatches.byMatchRate()', () => {

    it('Should return correct data', async () => {
      const matches = await connector.getMatches.byMatchRate(UID)
      const expectedMatches: Match[] = [
        {
          id: '0xc2',
          userName: 'amomi',
          state: 'FCT',
          country: 'NG',
          userImageUrl: '',
          hasGameNames: ['New Super Mario Bros. Deluxe!'],
          wantedGameNames: ['Destiny Warriors RPG']
        }, {
          id: '0xc4',
          userName: 'dioda',
          state: 'ENUGU',
          country: 'NG',
          userImageUrl: '',
          hasGameNames: ['New Super Mario Bros. Deluxe!', 'Remnant From The Ashes'],
          wantedGameNames: []
        }, {
          id: '0xc3',
          userName: 'obi3combo',
          state: '',
          country: '',
          userImageUrl: '',
          hasGameNames: [],
          wantedGameNames: ['Destiny Warriors RPG']
        }
      ]
      expect(matches.map(m => m.id)).to.deep.include.members(expectedMatches.map(m => m.id))
      expect(matches.length).to.be.equal(expectedMatches.length)
      // expect(matches.map(m => m.wantedGameNames)).deep.include.members(expectedMatches.map(m => m.wantedGameNames))
    })
  })

  describe('getMatches.byLocation()', () => {
    it('Should return correct data', async () => {
      const matches = await connector.getMatches.byLocation(UID)
      const expectedMatches: Match[] = [
        {
          id: '0xc2',
          userName: 'amomi',
          state: 'FCT',
          country: 'NG',
          userImageUrl: '',
          hasGameNames: ['New Super Mario Bros. Deluxe!'],
          wantedGameNames: ['Destiny Warriors RPG']
        }, {
          id: '0xc4',
          userName: 'dioda',
          state: 'ENUGU',
          country: 'NG',
          userImageUrl: '',
          hasGameNames: ['New Super Mario Bros. Deluxe!', 'Remnant From The Ashes'],
          wantedGameNames: []
        }, {
          id: '0xc5',
          userName: 'omega',
          state: 'FCT',
          country: 'NG',
          userImageUrl: '',
          hasGameNames: [],
          wantedGameNames: []
        }
      ]
      expect(matches.map(m => m.id)).to.deep.include.members(expectedMatches.map(m => m.id))
      expect(matches.length).to.be.equal(expectedMatches.length)

    })
  })

  describe('createDbGames()', () => {

    beforeEach(async () => {
      await connector.clearDb()
      await connector.seedDb()
    })

    it('Should create games', async () => {

      const clientStub = new dgraph.DgraphClientStub()
      const client = new dgraph.DgraphClient(clientStub)
      const transaction = client.newTxn()

      const gamesAlreadyAdded = await connector.createDbGames([{
        consoleType: 'ps4',
        imageUrl: 'gameimage',
        name: 'God Of War',
        popularity: 2
      }, {
        consoleType: 'ps3',
        imageUrl: 'gameimage',
        name: 'Tomb Raider',
        popularity: 1
      }, {
        consoleType: 'Linux',
        name: 'Overcome',
        imageUrl: '',
        popularity: 0
      }])

      const result = await GameExists('God Of War', 'ps4', transaction)
      const result1 = await GameExists('Tomb Raider', 'ps3', transaction)

      expect(result).to.be.true
      expect(result1).to.be.true
      expect(gamesAlreadyAdded.length).to.be.equal(1)
      expect(gamesAlreadyAdded[0]).to.be.deep.equal({
        consoleType: 'Linux',
        name: 'Overcome',
        imageUrl: '',
        popularity: 0
      })
    })

    it('Should create games with correct details')

    it('Should not create game if already exists', async () => {

    })
  })

  describe('searchDbGames()', () => {
    it('Should return an array of games with so results', async () => {
      const games = await connector.searchDbGames('super', 1)
      const gameNames = games.map(game => game.name)
      expect(['Super Fancy Pants Adventure', 'New Super Mario Bros. Deluxe!']).to.include.members(gameNames)
    })
    it('Should return results less than or equal to the limit specified', async () => {
      const games = await connector.searchDbGames('super', 1)
      expect(games.length).to.be.at.most(1)
    })
    it('Should return an empty array if no results found', async () => {
      const games = await connector.searchDbGames('susadfsfspsdfedr', 1)
      expect(games).to.be.an('Array')
      expect(games.length).to.be.equal(0)
    })
  })

  describe('getUserOfferStatus()', () => {

    it(`Should return null if user doesn't participate in offer`, async () => {
      const status = await connector.getUserOfferStatus(UID, NOT_PARTICIPATES_OFFER_ID)
      expect(status).to.be.null
    })

    it('Should return correct user offer status', async () => {
      const status = await connector.getUserOfferStatus(UID, PARTICIPATES_OFFER_ID)
      expect(status).to.be.equal('pending')
    })
    it('Should throw userId Empty', async () => {
      await expect(connector.getUserOfferStatus('', PARTICIPATES_OFFER_ID)).to.be.rejectedWith('userId Empty')

    })
    it('Should throw offerId Empty', async () => {
      await expect(connector.getUserOfferStatus(UID, '')).to.be.rejectedWith('offerId Empty')

    })
  })

  describe('setUserOfferStatus()', () => {
    it('Should throw userId Empty', async () => {
      await expect(connector.setUserOfferStatus('', 'sender', PARTICIPATES_OFFER_ID, 'cancelled'))
    })
    it('Should throw offerId Empty', async () => {
      await expect(connector.setUserOfferStatus(UID, 'sender', '', 'cancelled'))
    })

    it('Should change offer status correctly', async () => {
      // const { uid: offerId } = await connector.createOffer({ myGames: [{details}], otherGames: [], otherId: UID_OTHER }, UID)
      const updatedStatus = await connector.setUserOfferStatus(UID, 'sender', PARTICIPATES_OFFER_ID, 'cancelled')
      const offer = await connector.getUserOffers('pending', UID, { limit: 10, offset: 0 })
      const { senderStatus } = offer.result.filter(o => o.offerId === PARTICIPATES_OFFER_ID)[0]
      expect(senderStatus).to.be.equal('cancelled')
    })
  })

  describe('updateOffer()', () => {

    it('Should Update Offer With Correct Type', async () => {
      await connector.updateOffer(PARTICIPATES_OFFER_ID, 'completed')
      const offer = await connector.getOffer(PARTICIPATES_OFFER_ID, UID)
      expect(offer.status).to.be.equal('completed')
    })
  })

  describe('createOffer()', () => {
    const offer: MutationResolvers.CreateOfferMutationInput['offer'] = {
      otherId: '0xc2',
      myGames: OFFER_MY_GAMES,
      otherGames: OFFER_OTHER_GAMES
    }

    beforeEach(async () => {
      await connector.clearDb()
      await connector.seedDb()
    })

    it('should create a new offer', async () => {
      const initialOffers = (await connector.getUserOffers('pending', UID, { offset: 0, limit: 10 })).result
      const newOfferUid = (await connector.createOffer(offer, UID)).uid
      const currentOffers = (await connector.getUserOffers('pending', UID, { offset: 0, limit: 10 })).result

      expect(currentOffers.length).to.be.greaterThan(initialOffers.length)

      const result = await connector.getOffer(newOfferUid, UID)
      expect(result).to.not.be.null

    })

    it('created offer should have correct data', async () => {
      const newOfferUid = (await connector.createOffer(offer, UID)).uid

      const result = await connector.getOffer(newOfferUid, UID)

      expect(offer.otherId).to.be.equal(result.receiverId)
      const resultMyGamesIds = result.senderGames.map(g => g.game.id)
      const offerGameIds = offer.myGames.map(g => g.gameId)
      expect(offerGameIds).to.include.members(resultMyGamesIds)
      expect(offerGameIds.length).to.be.equal(resultMyGamesIds.length)

    })

    it('number of offers should not change if offer with same participants already exists', async () => {

      const offerEdited: MutationResolvers.CreateOfferMutationInput['offer'] = {
        otherId: '0xc3',
        myGames: OFFER_MY_GAMES_EDITED,
        otherGames: OFFER_OTHER_GAMES_EDITED
      }

      const initialOffers = (await connector.getUserOffers('pending', UID, { offset: 0, limit: 10 })).result

      await connector.createOffer(offerEdited, UID)

      const currentOffers = (await connector.getUserOffers('pending', UID, { offset: 0, limit: 10 })).result

      expect(initialOffers.length).to.be.equal(currentOffers.length)

    })
  })

  describe('getUserOffers()', () => {

    it('should throw userId empty if userId is empty string', () => {
      expect(connector.getUserOffers('cancelled', '', { limit: 0, offset: 0 })).to.be.rejectedWith('userId Empty')
    })
    it('should return correct offers', async () => {
      const pendingOffers = await connector.getUserOffers('pending', UID, { offset: 0, limit: 10 })
      expect(pendingOffers.result.length).to.be.equal(1)

      const pendingOffersIds = pendingOffers.result.map(offer => offer.offerId)
      expect(['0xb2', '0xb1']).to.include.members(pendingOffersIds)

      const completedOffers = await connector.getUserOffers('completed', UID, { offset: 0, limit: 10 })
      expect(completedOffers.result.length).to.be.equal(1)

      const completedOffersIds = completedOffers.result.map(offer => offer.offerId)
      expect(['0xb3']).to.include.members(completedOffersIds)
    })

    it('should return correct pageInfo', async () => {
      const pendingOffers = await connector.getUserOffers('pending', UID, { offset: 0, limit: 10 })
      expect(pendingOffers.pageInfo.noOfItems).to.be.equal(1)
    })

    it('pagination should not repeat result', async () => {
      const pendingOffersPage1 = await connector.getUserOffers('pending', UID, { offset: 0, limit: 1 })
      const pendingOffersPage2 = await connector.getUserOffers('pending', UID, { offset: 1, limit: 1 })
      const page1Ids = pendingOffersPage1.result.map(offer => offer.offerId)
      const page2Ids = pendingOffersPage2.result.map(offer => offer.offerId)
      expect(page2Ids).to.not.include.members(page1Ids)
    })

    it('should return empty array if no offers', async () => {
      const pendingOffers = await connector.getUserOffers('pending', UID, { offset: 0, limit: 10 })

    })
  })

  describe('getOffer()', () => {
    it('Should throw offerId empty if offerId is empty string', () => {

      expect(connector.getOffer('', UID)).to.be.rejectedWith('offerId Empty')
    })

    it('Should return null if offerid doesn\'t exist', async () => {
      const result = await connector.getOffer(NOT_PARTICIPATES_OFFER_ID, UID)
      expect(result).to.be.null
    })

    it('should return correct offer', async () => {

      const result = await connector.getOffer(PARTICIPATES_OFFER_ID, UID)
      expect(result).to.not.be.null
      expect(result.offerId).to.be.equal(PARTICIPATES_OFFER_ID)
      expect(result.status).to.be.equal('pending')
      expect(result.receiverId).to.be.equal(UID_OTHER)
      expect(result.epochTimeCreated).to.be.equal('2019-04-12T22:04:53Z')
    })
  })

  describe('getUser.withUid()', () => {
    it('Should return null if user doesn\'t exist', async () => {
      const user = await connector.getUser.withUid(UID_NOT_CREATED)
      expect(user).to.be.null
    })

    it('should return null if uid is an empty string', async () => {
      const user = await connector.getUser.withUid('')
      expect(user).to.be.null
    })

    it('should return a user if exists', async () => {
      const user = await connector.getUser.withUid(UID)

      expect(user).to.have.property('id')
      expect(user.id).to.be.equal(UID)
    })

    it('should return correct values', async () => {
      const user = await connector.getUser.withUid(UID)
      const expectedUser: User = {
        id: UID,
        info: {
          description: '`I have two Xbox systems and 4 2DS XL systems. All brand new sealed.`',
          email: 'sim04ful@gmail.com',
          userName: 'sim04ful',
          epochTimeCreated: '',
          isPro: true,
          location: {
            country: 'NG',
            state: 'FCT'
          },
          noOfSuccessfulExchanges: 10,
          rating: {
            positive: 10,
            negative: 1
          },
          userImageUrl: '',
          isBanned: false,
          setting_matchNotifications: true,
          user_subscriptionUpdateUrl: '',
          user_subscriptionCancelUrl: ''
        }
      }
      expect(user).to.deep.equal(expectedUser)
    })
  })

  describe('getUser.withUserName()', () => {
    it('Should return null if user doesn\'t exist', async () => {
      const user = await connector.getUser.withUserName(USER_NAME_NOT_CREATED)
      expect(user).to.be.null
    })

    it('should return null if userName is an empty string', async () => {
      const user = await connector.getUser.withUserName('')
      expect(user).to.be.null
    })

    it('should return a user if exists', async () => {
      const user = await connector.getUser.withUserName(USER_NAME)

      expect(user).to.have.property('id')
      expect(user.info.userName).to.be.equal(USER_NAME)

    })
  })

  describe('getUser.withUserEmail()', () => {
    it('Should return null if user doesn\'t exist', async () => {
      const user = await connector.getUser.withUserEmail(USER_EMAIL_NOT_CREATED)
      expect(user).to.be.null
    })

    it('should return null if userEmail is an empty string', async () => {
      const user = await connector.getUser.withUserEmail('')
      expect(user).to.be.null
    })

    it('should return a user if exists', async () => {
      const user = await connector.getUser.withUserEmail(USER_EMAIL)
      expect(user).to.have.property('id')
    })
  })

  describe(`getUser.withUserEmail() == getUser.withUid()`, () => {

    it('should return same user if uid and userEmail belong to same user', async () => {
      const userUsingUserEmail = await connector.getUser.withUserEmail(USER_EMAIL)
      const userUsingUid = await connector.getUser.withUid(UID)
      expect(userUsingUid).to.deep.equal(userUsingUserEmail)
    })

  })

  describe(`getUser.withUserName() == getUser.withUid()`, () => {

    it('should return same user if uid and userName belong to same user', async () => {
      const userUsingUserName = await connector.getUser.withUserName(USER_NAME)
      const userUsingUid = await connector.getUser.withUid(UID)
      expect(userUsingUid).to.deep.equal(userUsingUserName)
    })

  })

  describe('createUser()', () => {

    it('should throw user exists error if user with userEmail exists', async () => {
      await expect(connector.createUser(USER_EMAIL)).to.be.rejectedWith('User Exists')
    })

    it('should throw userId empty if userId is empty string', async () => {
      await expect(connector.createUser('')).to.be.rejectedWith('userEmail Empty')
    })

    it('should return a uid when user created', async () => {
      const { uid } = await connector.createUser(USER_EMAIL_NOT_CREATED)
      expect(uid).to.not.be.null

      const user = await connector.getUser.withUid(uid)
      expect(user).to.have.property('id')
      expect(user.id).to.be.equal(uid)
    })
  })

  describe('getUserGames()', () => {
    it('Should return only usergames with correct status', async () => {
      const hasUserGame = await connector.getUserGames(UID, 'has')
      const wantUserGame = await connector.getUserGames(UID, 'want')

      const hasUserGameCount = hasUserGame.filter((x) => x.details.status === 'has')
      const wantUserGameCount = wantUserGame.filter((x) => x.details.status === 'want')

      expect(hasUserGame.length).to.be.equal(hasUserGameCount.length)
      expect(wantUserGame.length).to.be.equal(wantUserGameCount.length)
    })

    it('Should return correct number of games', async () => {
      const hasUserGame = await connector.getUserGames(UID, 'has')
      const wantUserGame = await connector.getUserGames(UID, 'want')

      expect(hasUserGame.length).to.be.equal(1) // remember to update if seed changes
      expect(wantUserGame.length).to.be.equal(3)

      const wantUserGamesName = wantUserGame.map(game => game.game.name)
      expect(wantUserGamesName).to.contain('Custom Game 1')

    })

    it('Should return custom games with correct values', async () => {
      const wantUserGame = await connector.getUserGames(UID, 'want')
      // const wantUserGamesName = wantUserGame.map(game => game.game.name)
      expect(wantUserGame).to.deep.include({
        details: {
          description: '',
          status: 'want',
          tradeType: 'sale',
        },
        game: {
          consoleType: 'Custom Platform 1',
          id: 'Custom Game 1' + 'Custom Platform 1',
          imageUrl: '',
          name: 'Custom Game 1'
        },
        id: '0xab4'

      } as UserGame)
    })
  })

  describe('updateUser()', () => {
    it('Should Update Correct User', async () => {

      const userInfo = {
        description: 'updated user description',
        email: 'updated@email.com',
        location: { longitude: 10, latitude: 1 },
        latitiude: 1,
        longitude: 10,
        user_subscriptionCancelUrl: 'cancel_url',
        user_subscriptionUpdateUrl: 'update_url'
      } as MutationResolvers.UserInfoInput

      await connector.updateUser(userInfo, UID)
      const result = await (connector.getUser.withUid(UID))
      expect(result.info.description).to.be.equal(userInfo.description)
      expect(result.info.email).to.be.equal(userInfo.email)
      expect(result.info.user_subscriptionCancelUrl).to.be.equal(userInfo.user_subscriptionCancelUrl)
      expect(result.info.user_subscriptionUpdateUrl).to.be.equal(userInfo.user_subscriptionUpdateUrl)

    })
    it('Should throw userId empty if userId is empty string', () => {

      expect(connector.updateUser({}, '')).to.be.rejectedWith('userId Empty')
    })

  })

  describe('participatesInOffer()', () => {
    it('Should return true if userId participates in offer', async () => {
      const result = await connector.participatesInOffer(PARTICIPATES_OFFER_ID, UID)
      expect(result).to.be.equal(true)
    })

    it('Should return false if userId does not participate in offer', async () => {
      const result = await connector.participatesInOffer(NOT_PARTICIPATES_OFFER_ID, UID)
      expect(result).to.be.equal(false)
    })
  })

  describe('getNoOfUserGames()', () => {
    it('Should return correct values', async () => {
      const noOfWantedGames = await connector.getNoOfUserGames(UID, 'want')
      const noOfHasGames = await connector.getNoOfUserGames(UID, 'has')
      expect(noOfWantedGames).to.be.equal(3)
      expect(noOfHasGames).to.be.equal(1)

    })
  })

  describe('getUserCount', () => {
    it('Should return a number', async () => {
      const count = await connector.getUserCount()
      expect(count).to.be.equal(5)
    })
  })
})
