import dgraph = require('dgraph-js')
import { Game } from 'src/types/models';

interface GameDb {
    'game.coverUrl': string,
    'game.name': string,
    'game.platform': string
    uid: string
}

const parseDbGameToGame = (game: GameDb): Game => {
    return {
        consoleType: game['game.platform'],
        id: game.uid,
        imageUrl: game['game.coverUrl'],
        name: game['game.name']
    }
}

export default async (searchText: string, limit: number = 10, client: dgraph.DgraphClient): Promise<Game[]> => {
    const transaction = client.newTxn()
    const query = `
    {
        games(func:has(game), orderdesc:game.popularity,first:${limit}) @filter(alloftext(game.name,"${searchText}")) {
         game.coverUrl
         game.name
         game.platform
         uid
 }}
    `
    try {
        const json = (await transaction.query(query)).getJson()
        const gamesDb: GameDb[] = !!json.games ? json.games : []
        return gamesDb.map(parseDbGameToGame)
    } catch (error) {
        console.error(error)
        throw error
    } finally {
        transaction.discard()
    }
}