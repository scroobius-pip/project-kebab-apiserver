import dgraph = require('dgraph-js')
import { MutationResolvers } from 'src/.generated/graphqlgen';
import parseUserGameInputToAddGameDb, { AddGameDb } from './_utils/parseUserGameInputToAddGameDb';

export default async (
    games: MutationResolvers.AddGamesInput[],
    userId: string,
    client: dgraph.DgraphClient): Promise<void> => {

    if (!games.length) { return }

    const transaction = client.newTxn()
    try {
        const mutation = new dgraph.Mutation()
        const data: AddGameDb = parseUserGameInputToAddGameDb(games, userId)
        mutation.setSetJson(data)
        await transaction.mutate(mutation)
        await transaction.commit()

    } catch (error) {
        console.error(error)
        throw error
    } finally {
        transaction.discard()
    }
}
