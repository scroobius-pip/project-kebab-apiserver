import dgraph = require('dgraph-js')
import { MutationResolvers } from 'src/.generated/graphqlgen';
import parseRemoveGameInputToRemoveGameDb, { RemoveGameDb } from './_utils/parseRemoveGameInputToRemoveGameDb';

const removeGames = async (
    games: MutationResolvers.RemoveGamesInput[],
    uid: string,
    client: dgraph.DgraphClient): Promise<void> => {

    if (!games.length) { return }

    const transaction = client.newTxn()
    try {
        const mutation = new dgraph.Mutation()
        const data: RemoveGameDb = parseRemoveGameInputToRemoveGameDb(games, uid)
        mutation.setDeleteJson(data)
        await transaction.mutate(mutation)
        await transaction.commit()

    } catch (error) {
        console.error(error)
        throw error
    } finally {
        transaction.discard()
    }
}

export default removeGames