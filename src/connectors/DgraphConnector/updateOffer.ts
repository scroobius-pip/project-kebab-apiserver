import { OfferStatus } from 'src/.generated/graphqlgen';

import dgraph = require('dgraph-js')

export default async (
    offerId: string,
    status: OfferStatus,
    client: dgraph.DgraphClient): Promise<void> => {

    if (!offerId.length) { throw new Error('offerId Empty') }

    const transaction = client.newTxn()
    try {
        const mutation = new dgraph.Mutation()
        const data = [{
            uid: offerId,
            'offer.status': status
        }]
        mutation.setSetJson(data)
        await transaction.mutate(mutation)
        await transaction.commit()

    } catch (error) {
        console.error(error)
        throw error
    } finally {
        transaction.discard()
    }

}