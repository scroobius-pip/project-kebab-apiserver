import dgraph = require('dgraph-js');
import userExists from './_utils/userExists';

export default async (userEmail: string, client: dgraph.DgraphClient) => {
    if (!userEmail.length) { throw new Error(`userEmail Empty`) }

    const userData = {
        uid: '_:user',
        'user.email': userEmail,
        user: '', // empty user property used as a node label in dgraph
        'user.epochTimeCreated': ((new Date()).toISOString())
    }

    const transaction = client.newTxn()

    try {

        if ((await userExists(userEmail, transaction))) {
            throw new Error('User Exists');
        };

        const mutation = new dgraph.Mutation()
        mutation.setSetJson(userData)

        const result: any = await transaction.mutate(mutation)
        await transaction.commit()
        const uidsMap = result.getUidsMap().forEach((uid: string, key: string) => {
            console.log(`${uid} ${key}`)
        });
        return { uid: result.getUidsMap().get('user') }
    } catch (error) {
        console.error(error)
        throw error

    } finally {
        transaction.discard()
    }
}
