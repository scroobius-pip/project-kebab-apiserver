import dgraph = require('dgraph-js')
import { data, schema } from './seed'

export default async (client: dgraph.DgraphClient) => {
    try {
        // set schema
        const op = new dgraph.Operation()
        op.setSchema(schema)
        await client.alter(op)

        // set mock data
        const transaction = client.newTxn()
        try {

            const mutation = new dgraph.Mutation()

            mutation.setSetJson(data)
            await transaction.mutate(mutation)
            await transaction.commit()

        } finally {
            transaction.discard()
        }

    } catch (error) {
        console.error(error)
        throw new Error(error)
    }
}