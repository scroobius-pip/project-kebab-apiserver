import dgraph = require('dgraph-js')
import { MutationResolvers } from 'src/.generated/graphqlgen';
import { UpdateGamesMutationResult } from 'src/types/models';
import getAllUserGames from './_utils/getAllUserGames';
import DgraphConnector from '.';
import parseUpdateGameInputToUpdateGameDb, { UpdateGameDb } from './_utils/parseUpdateGameInputToUpdateGameDb';

const updateGames = async (
    games: MutationResolvers.UpdateGamesMutationInput['games'],
    uid: string,
    client: dgraph.DgraphClient
): Promise<void> => {
    if (!games.length) { return }

    const transaction = client.newTxn()
    try {
        const mutation = new dgraph.Mutation()
        const data = parseUpdateGameInputToUpdateGameDb(games, uid)
        mutation.setSetJson(data)
        await transaction.mutate(mutation)
        await transaction.commit()

    } catch (error) {
        console.error(error)
        throw error
    } finally {
        transaction.discard()
    }
}

export default updateGames