import { DgraphClient } from 'dgraph-js';
import { UserGameDetailsTradeType, OfferStatus } from 'src/.generated/graphqlgen';
import parseDbOfferToOffer, { OfferDb } from './_utils/parseDbOfferToOffer';

export default async (offerId: string, userId: string, client: DgraphClient) => {

  if (!userId.length) { throw new Error('userId Empty') }
  if (!offerId.length) { throw new Error('offerId Empty') }

  const transaction = client.newTxn()
  const query = `
    {
      offer(func:uid(${offerId})) {
      uid
      epochTimeCreated:offer.epochTimeCreated
        offerId:offer.offerId
          status:offer.status
          receiverGames:offer.receiverGames {
            description
            tradeType
            uid
            info {
              name:game.name
              consoleType:game.platform
              imageUrl:game.coverUrl
              uid
            }
          }
          senderGames:offer.senderGames {
             description
            tradeType
            uid
            info {
              name:game.name
              consoleType:game.platform
              imageUrl:game.coverUrl
              uid
            }
          }
        receiverStatus:~user.participates @facets(eq(type,"receiver")) @facets(status:status){uid}
            senderStatus:~user.participates @facets(eq(type,"sender")) @facets(status:status){uid}
        }
    }
  `
  try {

    const json = (await transaction
      .query(query)).getJson()

    if (!json.offer.length) {
      return null
    }
    const offer: OfferDb = json.offer[0]
    if (!offer.status) { return null }
    const result = parseDbOfferToOffer(offer, userId)
    return result
  } catch (error) {
    console.error(error)
    throw error

  } finally {
    transaction.discard()
  }
}
