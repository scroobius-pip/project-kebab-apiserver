import { OfferStatus } from 'src/.generated/graphqlgen';

import dgraph = require('dgraph-js')

export default async (userId: string, type: 'receiver' | 'sender', offerId: string, status: OfferStatus, client: dgraph.DgraphClient) => {
    if (!userId.length) { throw new Error('userId Empty') }
    if (!offerId.length) { throw new Error('offerId Empty') }

    const transaction = client.newTxn()
    try {
        const mutation = new dgraph.Mutation()
        const data = [{
            uid: userId,
            'user.participates':
            {
                uid: offerId,
                'user.participates|status': status,
                'user.participates|type': type
            }

        }]
        mutation.setSetJson(data)
        await transaction.mutate(mutation)
        await transaction.commit()
        return status
    } catch (error) {
        console.error(error)
        throw error
    } finally {
        transaction.discard()
    }

}