import { DgraphClient } from 'dgraph-js';
import parseDbUserToUser, { DbUser } from './_utils/parseDbUserToUser';

const userQueryFragment = `
uid
user.userName
user.epochTimeCreated
user.isPro
user.isBanned
user.location
user.noOfSuccessfulExchanges
user.participates
user.ratingPositive
user.ratingNegative
user.userImageUrl
user.description
user.email
user.country
user.state
user.matchNotifications
user.subscriptionUpdateUrl
user.subscriptionCancelUrl
`


const withUid = async (uid: string, client: DgraphClient) => {
  if (!uid.length) { return null }
  const transaction = client.newTxn()
  try {
    const query = `query user($userId: string){
        user(func: uid($userId)){
         ${userQueryFragment}
        }
      }`

    const vars = {
      $userId: uid
    }
    const result: DbUser = (await transaction.queryWithVars(query, vars)).getJson().user[0]
    if (!result['user.email']) { return null }

    return parseDbUserToUser(result)

  } catch (error) {
    console.error(error)
    throw error
  }
}

const withUserName = async (userName: string, client: DgraphClient) => {
  if (!userName.length) { return null }
  const transaction = client.newTxn()
  try {
    const query = `query user($userName: string){
        user(func: eq(user.userName, $userName)){
         ${userQueryFragment}
        }
      }`

    const vars = {
      $userName: userName
    }
    const result: DbUser = (await transaction.queryWithVars(query, vars)).getJson().user[0]
    if (!result) { return null }
    return parseDbUserToUser(result)

  } catch (error) {
    console.error(error)
    throw error
  }
}

const withUserEmail = async (userEmail: string, client: DgraphClient) => {
  if (!userEmail.length) { return null }
  const transaction = client.newTxn()
  try {
    const query = `query user($email: string){
        user(func: eq(user.email, $email)){
      ${userQueryFragment}
        }
      }`

    const vars = {
      $email: userEmail
    }
    const result: DbUser = (await transaction.queryWithVars(query, vars)).getJson().user[0]
    if (!result) { return null }
    return parseDbUserToUser(result)

  } catch (error) {
    console.error(error)
    throw error
  } finally {
    transaction.discard()
  }
}

export default { withUid, withUserName, withUserEmail }