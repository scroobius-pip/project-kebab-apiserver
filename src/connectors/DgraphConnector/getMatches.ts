import { DgraphClient } from 'dgraph-js'
import { Match } from 'src/types/models';
import parseDbMatchToMatch, { DbMatch } from './_utils/parseDbMatchToMatch';

const matchRateQuery = `
query matches($userId:string){var(func: uid($userId)){user.game @facets(eq(status,"want")){W1 as info {~info {USERS_W as  ~user.game@filter(NOT uid($userId))}}}}var(func: uid($userId)){user.game @facets(eq(status,"has")){H1 as	 info {
    ~info {
    USERS_H as  ~user.game@filter(NOT uid($userId))}}}}var(func:uid(USERS_W)){user.game@facets(eq(status,"has")) {H2_COUNT as count(info @filter(uid(W1)))info@filter(uid(W1)) {H2_NAME as game.name}}H2_SUM as sum(val(H2_COUNT))}var(func:uid(USERS_H)){user.game@facets(eq(status,"want")){W2_COUNT as count(info @filter(uid(H1)))info@filter(uid(H1)) {W2_NAME as game.name}}W2_SUM as sum(val(W2_COUNT))}USERS as var	(func:uid(USERS_H,USERS_W)){score as math(H2_SUM+W2_SUM)}recommended(func:uid(USERS),orderdesc:val(score))@filter(has(user.userName)) {uid
    user.userName
    user.state
    user.country
    user.userImageUrl
    score:val(score)
    user.game {
    val(H2_NAME)
    info {
    has: val(H2_NAME)
    wants: val(W2_NAME)}}}}
`

const locationQuery = `
query matches($userId:string){var(func:uid($userId)){
    state as user.state 
    country as user.country 
    user.game @facets(eq(status,"want")){W1 as info
}}var(func:uid($userId)){user.game @facets(eq(status,"has")){H1 as	info
}}var (func:eq(user.state,val(state)))@filter(NOT uid($userId)){USERS_S as  uid}var (func:eq(user.country,val(country)))@filter(NOT uid($userId)){USERS_C as  uid}var (func:uid(USERS_S,USERS_C)){USERS as  uid
}var(func:uid(USERS)){user.game@facets(eq(status,"has")) {H2_COUNT as count(info @filter(uid(W1)))info@filter(uid(W1)) {H2_NAME as game.name}}H2_SUM as sum(val(H2_COUNT))}var(func:uid(USERS)){user.game@facets(eq(status,"want")){W2_COUNT as count(info @filter(uid(H1)))info@filter(uid(H1)) {W2_NAME as game.name}}W2_SUM as sum(val(W2_COUNT))}var(func:uid(USERS)){score as math(H2_SUM+W2_SUM)}recommended(func:uid(USERS),orderdesc:val(score))@filter(has(user.userName)) {uid
user.userName
user.userImageUrl
user.country
user.state
score:val(score)user.game {
val(H2_NAME)
info {
has: val(H2_NAME)wants: val(W2_NAME)}}}}
`
const getMatchesWithQuery = async (userId: string, client: DgraphClient, matchQuery: string): Promise<Match[]> => {
    if (!userId.length) { throw new Error('userId Empty') }

    const transaction = client.newTxn()
    try {
        const vars = {
            $userId: userId
        }
        const dbMatches: DbMatch[] = (await transaction.queryWithVars(matchQuery, vars)).getJson()?.recommended
        if (!dbMatches)
            return []
        return dbMatches.map(parseDbMatchToMatch)
    } catch (error) {
        console.error(error)
        throw error
    } finally {
        transaction.discard()
    }
}

const byLocation = async (userId: string, client: DgraphClient): Promise<Match[]> =>
    await getMatchesWithQuery(userId, client, locationQuery)

const byMatchRate = async (userId: string, client: DgraphClient): Promise<Match[]> =>
    await getMatchesWithQuery(userId, client, matchRateQuery)

export default { byLocation, byMatchRate }
