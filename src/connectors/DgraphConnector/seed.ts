const schema =
  `
  <description>: string .
  <game.coverUrl>: string .
  <game.name>: string @index(fulltext, hash) @upsert .
  <game.platform>: string .
  <game.popularity>: float .
  <game.url>: string .
  <game.uuid>: string @index(hash) .
  <game>: string .
  <info>: [uid] @reverse .
  <name>: string .
  <negative>: int .
  <offer.epochTimeCreated>: datetime @index(hour) .
  <offer.offerId>: string @index(hash) .
  <offer.receiverGames>: [uid] .
  <offer.receiverId>: string .
  <offer.senderGames>: [uid] .
  <offer.senderId>: string .
  <offer.status>: string @index(hash) .
  <offer>: string .
  <platform>: string .
  <positive>: int .
  <tradeType>: string .
  <user.country>: string @index(hash) .
  <user.description>: string .
  <user.email>: string @index(hash) @upsert .
  <user.epochTimeCreated>: string .
  <user.game>: [uid] @reverse .
  <user.gamesHas>: [uid] .
  <user.gamesWant>: [uid] .
  <user.isBanned>: bool .
  <user.isPro>: bool .
  <user.latitude>: float .
  <user.location>: geo .
  <user.longitude>: float .
  <user.matchNotifications>: bool .
  <user.noOfSuccessfulExchanges>: int .
  <user.participates>: [uid] @reverse .
  <user.ratingNegative>: int .
  <user.ratingPositive>: int .
  <user.state>: string @index(hash) .
  <user.userImageUrl>: string .
  <user.userName>: string @index(hash) .
  <user.subscriptionUpdateUrl>: string .
  <user.subscriptionCancelUrl>: string .
  <user>: string .
`

const data = {
  "set": [
    {
      "game": "",
      "uid": "0xa1",
      "game.name": "Destiny Warriors RPG",
      "game.coverUrl": "//images.igdb.com/igdb/image/upload/t.thumb/x5mdtvs8inja59urkl4p.jpg",
      "game.popularity": 1.333333333333333,
      "game.url": "https://www.igdb.com/games/destiny.warriors.rpg",
      "game.uuid": "DestinyWarriorsRPGPC(MicrosoftWindows)",
      "game.platform": "PC (Microsoft Windows)"
    },
    {
      "game": "",
      "uid": "0xa2",
      "game.name": "New Super Mario Bros. Deluxe!",
      "game.coverUrl": "//images.igdb.com/igdb/image/upload/t.thumb/bwfdndr7jmy9uwxj0llu.jpg",
      "game.popularity": 1,
      "game.url": "https://www.igdb.com/games/new.super.mario.bros.deluxe",
      "game.uuid": "NewSuperMarioBros.Deluxe!NintendoDS",
      "game.platform": "Nintendo DS"
    },
    {
      "game": "",
      "uid": "0xa3",
      "game.name": "SoulHunt",
      "game.coverUrl": "//images.igdb.com/igdb/image/upload/t.thumb/civeabvpfnq86kaqbj55.jpg",
      "game.popularity": 1.333333333333333,
      "game.url": "https://www.igdb.com/games/soulhunt",
      "game.platform": "Linux",
      "game.uuid": "SoulHuntLinux"
    },
    {
      "game": "",
      "uid": "0xa4",
      "game.name": "Remnant From The Ashes",
      "game.coverUrl": "https://images.igdb.com/igdb/image/upload/t.cover.big/xfmxhs5soeodr4wxnn4r.jpg",
      "game.popularity": 1.333333333333333,
      "game.url": "https://www.igdb.com/games/remnant.from.the.ashes",
      "game.platform": "Linux",
      "game.uuid": "RemnantFromTheAshesLinux"
    },
    {
      "game": "",
      "uid": "0xa5",
      "game.name": "Overcome",
      "game.coverUrl": "",
      "game.popularity": 1.333333333333333,
      "game.url": "https://www.igdb.com/games/remnant.from.the.ashes",
      "game.platform": "Linux",
      "game.uuid": "OvercomeLinux"
    },
    {
      "game": "",
      "game.name": "Super Fancy Pants Adventure",
      "game.coverUrl": "//images.igdb.com/igdb/image/upload/t_thumb/yblzds9r0gb8muchvoz7.jpg",
      "game.platform": "PC (Microsoft Windows)",
      "game.popularity": 27.66666666666667
    },
    {
      "game": "",
      "game.name": "Superola and the Lost Burgers",
      "game.coverUrl": "//images.igdb.com/igdb/image/upload/t_thumb/hfybvjh4xuhmvjkabqqq.jpg",
      "game.platform": "Nintendo Switch",
      "game.popularity": 1.33333333333
    },
    {
      "offer": "",
      "uid": "0xb2",
      "offer.offerId": "0xc10xc3",
      "offer.status": "pending",
      "offer.epochTimeCreated": "2019-04-12T22:04:53+00:00",
      "offer.senderGames": [
        {
          "tradeType": "swap",
          "description": "",
          "info": {
            "uid": "0xa1"
          }
        }
      ],
      "offer.receiverGames": [
        {
          "tradeType": "swap",
          "description": "",
          "info": {
            "uid": "0xa3"
          }
        }
      ]
    },
    {
      "offer": "",
      "uid": "0xb3",
      "offer.offerId": "0xc10xc4",
      "offer.status": "completed",
      "offer.epochTimeCreated": "2019-01-20T01:10:10+00:00",
      "offer.senderGames": [
        {
          "tradeType": "swap",
          "description": "",
          "info": {
            "uid": "0xa1"
          }
        }
      ],
      "offer.receiverGames": [
        {
          "tradeType": "swap",
          "description": "",
          "info": {
            "uid": "0xa3"
          }
        }
      ]
    },
    {
      "user": "",
      "uid": "0xc1",
      "user.description": "`I have two Xbox systems and 4 2DS XL systems. All brand new sealed.`",
      "user.email": "sim04ful@gmail.com",
      "user.userName": "sim04ful",
      "user.epochTimeCreated": "",
      "user.isPro": true,
      "user.matchNotifications": true,
      "user.isBanned": false,
      "user.longitude": 7,
      "user.latitude": 9.0765,
      "user.location": {
        "type": "Point",
        "coordinates": [
          7,
          9.0765
        ]
      },
      "user.country": "NG",
      "user.state": "FCT",
      "user.noOfSuccessfulExchanges": 10,
      "user.ratingPositive": 10,
      "user.ratingNegative": 1,
      "user.userImageUrl": "",
      "user.game": [
        {
          "user.game|status": "has",
          "uid": "0xab1",
          "tradeType": "swap",
          "description": "",
          "info": {
            "uid": "0xa1"
          }
        },
        {
          "user.game|status": "want",
          "uid": "0xab2",
          "tradeType": "sale",
          "description": "",
          "info": {
            "uid": "0xa2"
          }
        },
        {
          "user.game|status": "want",
          "uid": "0xab3",
          "tradeType": "sale",
          "description": "",
          "info": {
            "uid": "0xa4"
          }
        },
        {
          "user.game|status": "want",
          "uid": "0xab4",
          "tradeType": "sale",
          "description": "",
          "name": "Custom Game 1",
          "platform": "Custom Platform 1"

        }
      ],
      "user.participates": [

        {
          "user.participates|status": "pending",
          "user.participates|type": "sender",
          "uid": "0xb2"
        },
        {
          "user.participates|status": "completed",
          "user.participates|type": "sender",
          "uid": "0xb3"
        }
      ]
    },
    {
      "user": "",
      "uid": "0xc2",
      "user.email": "amomi@gmail.com",
      "user.userName": "amomi",
      "user.description": "Hello i am a chicken",
      "user.epochTimeCreated": "",
      "user.isPro": true,
      "user.matchNotifications": false,
      "user.isBanned": false,
      "user.longitude": 6,
      "user.latitude": 9.0765,
      "user.location": {
        "type": "Point",
        "coordinates": [
          6,
          9.0765
        ]
      },
      "user.country": "NG",
      "user.state": "FCT",
      "user.noOfSuccessfulExchanges": 10,
      "user.ratingPositive": 10,
      "user.ratingNegative": 1,
      "user.userImageUrl": "",
      "user.game": [
        {
          "user.game|status": "has",
          "tradeType": "sale",
          "description": "",
          "info": {
            "uid": "0xa3"
          }
        },
        {
          "user.game|status": "has",
          "tradeType": "sale",
          "description": "",
          "info": {
            "uid": "0xa2"
          }
        },
        {
          "user.game|status": "want",
          "tradeType": "swap",
          "description": "",
          "info": {
            "uid": "0xa1"
          }
        }
      ]

    },
    {
      "user": "",
      "uid": "0xc3",
      "user.email": "obinna@gmail.com",
      "user.userName": "obi3combo",
      "user.description": "Hello i am a turkey",
      "user.epochTimeCreated": "",
      "user.isPro": true,
      "user.matchNotifications": false,
      "user.isBanned": false,
      "user.longitude": 7.2,
      "user.latitude": 9.0765,
      "user.location": {
        "type": "Point",
        "coordinates": [
          7.2,
          9.0765
        ]
      },
      "user.country": "",
      "user.state": "",
      "user.noOfSuccessfulExchanges": 10,
      "user.ratingPositive": 10,
      "user.ratingNegative": 1,
      "user.participates": [
        {
          "user.participates|status": "pending",
          "user.participates|type": "receiver",
          "uid": "0xb2"
        }
      ],
      "user.game": [

        {
          "user.game|status": "want",
          "tradeType": "swap",
          "description": "",
          "info": {
            "uid": "0xa1"
          }
        }
      ]
    },
    {
      "user": "",
      "uid": "0xc4",
      "user.email": "dio@gmail.com",
      "user.userName": "dioda",
      "user.description": "But it was me dio!!",
      "user.epochTimeCreated": "",
      "user.isPro": false,
      "user.matchNotifications": false,
      "user.location": {
        "type": "Point",
        "coordinates": [
          122.4220186,
          37.772318
        ]
      },
      "user.country": "NG",
      "user.state": "ENUGU",
      "user.noOfSuccessfulExchanges": 10,
      "user.ratingPositive": 10,
      "user.ratingNegative": 1,
      "user.participates": [
        {
          "user.participates|status": "completed",
          "user.participates|type": "receiver",
          "uid": "0xb3"
        }
      ],
      "user.game": [

        {
          "user.game|status": "has",
          "tradeType": "sale",
          "description": "",
          "info": {
            "uid": "0xa2"
          }
        },
        {
          "user.game|status": "has",
          "tradeType": "sale",
          "description": "",
          "info": {
            "uid": "0xa4"
          }
        }
      ]
    },
    {
      "user": "",
      "uid": "0xc5",
      "user.email": "omega@gmail.com",
      "user.userName": "omega",
      "user.description": "Omega omega",
      "user.epochTimeCreated": "",
      "user.isPro": false,
      "user.matchNotifications": false,
      "user.location": {
        "type": "Point",
        "coordinates": [
          122.4220186,
          37.772318
        ]
      },
      "user.country": "NG",
      "user.state": "FCT",
      "user.noOfSuccessfulExchanges": 10,
      "user.ratingPositive": 10,
      "user.ratingNegative": 1,
      "user.participates": [
        {
          "user.participates|status": "completed",
          "user.participates|type": "receiver",
          "uid": "0xb3"
        }
      ],
      "user.game": [

        {
          "user.game|status": "has",
          "tradeType": "sale",
          "description": "",
          "info": {
            "uid": "0xa3"
          }
        }
      ]
    }
  ]
}

export { schema, data }

// {
//   "set": [
//     {
//       "game": "",
//       "uid": "_:game1",
//       "game.name": "Destiny Warriors RPG",
//       "game.coverUrl": "//images.igdb.com/igdb/image/upload/t.thumb/x5mdtvs8inja59urkl4p.jpg",
//       "game.popularity": 1.333333333333333,
//       "game.url": "https://www.igdb.com/games/destiny.warriors.rpg",
//       "game.uuid": "DestinyWarriorsRPGPC(MicrosoftWindows)",
//       "game.platform": "PC (Microsoft Windows)"
//     },
//     {
//       "game": "",
//       "uid": "_:game2",
//       "game.name": "New Super Mario Bros. Deluxe!",
//       "game.coverUrl": "//images.igdb.com/igdb/image/upload/t.thumb/bwfdndr7jmy9uwxj0llu.jpg",
//       "game.popularity": 1,
//       "game.url": "https://www.igdb.com/games/new.super.mario.bros.deluxe",
//       "game.uuid": "NewSuperMarioBros.Deluxe!NintendoDS",
//       "game.platform": "Nintendo DS"
//     },
//     {
//       "game": "",
//       "uid": "_:game3",
//       "game.name": "SoulHunt",
//       "game.coverUrl": "//images.igdb.com/igdb/image/upload/t.thumb/civeabvpfnq86kaqbj55.jpg",
//       "game.popularity": 1.333333333333333,
//       "game.url": "https://www.igdb.com/games/soulhunt",
//       "game.platform": "Linux",
//       "game.uuid": "SoulHuntLinux"
//     },
//     {
//       "game": "",
//       "uid": "_:game4",
//       "game.name": "Remnant From The Ashes",
//       "game.coverUrl": "https://images.igdb.com/igdb/image/upload/t.cover.big/xfmxhs5soeodr4wxnn4r.jpg",
//       "game.popularity": 1.333333333333333,
//       "game.url": "https://www.igdb.com/games/remnant.from.the.ashes",
//       "game.platform": "Linux",
//       "game.uuid": "RemnantFromTheAshesLinux"
//     },
//     {
//       "game": "",
//       "uid": "_:game5",
//       "game.name": "Overcome",
//       "game.coverUrl": "",
//       "game.popularity": 1.333333333333333,
//       "game.url": "https://www.igdb.com/games/remnant.from.the.ashes",
//       "game.platform": "Linux",
//       "game.uuid": "OvercomeLinux"
//     },
//     {
//       "game": "",
//       "game.name": "Super Fancy Pants Adventure",
//       "game.coverUrl": "//images.igdb.com/igdb/image/upload/t_thumb/yblzds9r0gb8muchvoz7.jpg",
//       "game.platform": "PC (Microsoft Windows)",
//       "game.popularity": 27.66666666666667
//     },
//     {
//       "game": "",
//       "game.name": "Superola and the Lost Burgers",
//       "game.coverUrl": "//images.igdb.com/igdb/image/upload/t_thumb/hfybvjh4xuhmvjkabqqq.jpg",
//       "game.platform": "Nintendo Switch",
//       "game.popularity": 1.33333333333
//     },
//     {
//       "offer": "",
//       "uid": "_:offer1",
//       "offer.offerId": "0xc10xc3",
//       "offer.status": "pending",
//       "offer.epochTimeCreated": "2019-04-12T22:04:53+00:00",
//       "offer.senderGames": [
//         {
//           "tradeType": "swap",
//           "description": "",
//           "info": {
//             "uid": "_:game1"
//           }
//         }
//       ],
//       "offer.receiverGames": [
//         {
//           "tradeType": "swap",
//           "description": "",
//           "info": {
//             "uid": "_:game3"
//           }
//         }
//       ]
//     },
//     {
//       "offer": "",
//       "uid": "_:offer2",
//       "offer.offerId": "0xc10xc4",
//       "offer.status": "completed",
//       "offer.epochTimeCreated": "2019-01-20T01:10:10+00:00",
//       "offer.senderGames": [
//         {
//           "tradeType": "swap",
//           "description": "",
//           "info": {
//             "uid": "_:game1"
//           }
//         }
//       ],
//       "offer.receiverGames": [
//         {
//           "tradeType": "swap",
//           "description": "",
//           "info": {
//             "uid": "_:game3"
//           }
//         }
//       ]
//     },
//     {
//       "user": "",
//       "uid": "_:user1",
//       "user.description": "`I have two Xbox systems and 4 2DS XL systems. All brand new sealed.`",
//       "user.email": "sim04ful@gmail.com",
//       "user.userName": "sim04ful",
//       "user.epochTimeCreated": "",
//       "user.isPro": true,
//       "user.matchNotifications": true,
//       "user.isBanned": false,
//       "user.longitude": 7,
//       "user.latitude": 9.0765,
//       "user.location": {
//         "type": "Point",
//         "coordinates": [
//           7,
//           9.0765
//         ]
//       },
//       "user.country": "NG",
//       "user.state": "FCT",
//       "user.noOfSuccessfulExchanges": 10,
//       "user.ratingPositive": 10,
//       "user.ratingNegative": 1,
//       "user.userImageUrl": "",
//       "user.game": [
//         {
//           "user.game|status": "has",
//           "uid": "_:usergame1",
//           "tradeType": "swap",
//           "description": "",
//           "info": {
//             "uid": "_:game1"
//           }
//         },
//         {
//           "user.game|status": "want",
//           "uid": "0xab2",
//           "tradeType": "sale",
//           "description": "",
//           "info": {
//             "uid": "_:game2"
//           }
//         },
//         {
//           "user.game|status": "want",
//           "uid": "0xab3",
//           "tradeType": "sale",
//           "description": "",
//           "info": {
//             "uid": "_:game4"
//           }
//         },
//         {
//           "user.game|status": "want",
//           "uid": "0xab4",
//           "tradeType": "sale",
//           "description": "",
//           "name": "Custom Game 1",
//           "platform": "Custom Platform 1"

//         }
//       ],
//       "user.participates": [

//         {
//           "user.participates|status": "pending",
//           "user.participates|type": "sender",
//           "uid": "_:offer1"
//         },
//         {
//           "user.participates|status": "completed",
//           "user.participates|type": "sender",
//           "uid": "_:offer2"
//         }
//       ]
//     },
//     {
//       "user": "",
//       "uid": "_:user2",
//       "user.email": "amomi@gmail.com",
//       "user.userName": "amomi",
//       "user.description": "Hello i am a chicken",
//       "user.epochTimeCreated": "",
//       "user.isPro": true,
//       "user.matchNotifications": false,
//       "user.isBanned": false,
//       "user.longitude": 6,
//       "user.latitude": 9.0765,
//       "user.location": {
//         "type": "Point",
//         "coordinates": [
//           6,
//           9.0765
//         ]
//       },
//       "user.country": "NG",
//       "user.state": "FCT",
//       "user.noOfSuccessfulExchanges": 10,
//       "user.ratingPositive": 10,
//       "user.ratingNegative": 1,
//       "user.userImageUrl": "",
//       "user.game": [
//         {
//           "user.game|status": "has",
//           "tradeType": "sale",
//           "description": "",
//           "info": {
//             "uid": "_:game3"
//           }
//         },
//         {
//           "user.game|status": "has",
//           "tradeType": "sale",
//           "description": "",
//           "info": {
//             "uid": "_:game2"
//           }
//         },
//         {
//           "user.game|status": "want",
//           "tradeType": "swap",
//           "description": "",
//           "info": {
//             "uid": "_:game1"
//           }
//         }
//       ]

//     },
//     {
//       "user": "",
//       "uid": "_:user3",
//       "user.email": "obinna@gmail.com",
//       "user.userName": "obi3combo",
//       "user.description": "Hello i am a turkey",
//       "user.epochTimeCreated": "",
//       "user.isPro": true,
//       "user.matchNotifications": false,
//       "user.isBanned": false,
//       "user.longitude": 7.2,
//       "user.latitude": 9.0765,
//       "user.location": {
//         "type": "Point",
//         "coordinates": [
//           7.2,
//           9.0765
//         ]
//       },
//       "user.country": "",
//       "user.state": "",
//       "user.noOfSuccessfulExchanges": 10,
//       "user.ratingPositive": 10,
//       "user.ratingNegative": 1,
//       "user.participates": [
//         {
//           "user.participates|status": "pending",
//           "user.participates|type": "receiver",
//           "uid": "_:offer1"
//         }
//       ],
//       "user.game": [

//         {
//           "user.game|status": "want",
//           "tradeType": "swap",
//           "description": "",
//           "info": {
//             "uid": "_:game1"
//           }
//         }
//       ]
//     },
//     {
//       "user": "",
//       "uid": "_:user4",
//       "user.email": "dio@gmail.com",
//       "user.userName": "dioda",
//       "user.description": "But it was me dio!!",
//       "user.epochTimeCreated": "",
//       "user.isPro": false,
//       "user.matchNotifications": false,
//       "user.location": {
//         "type": "Point",
//         "coordinates": [
//           122.4220186,
//           37.772318
//         ]
//       },
//       "user.country": "NG",
//       "user.state": "ENUGU",
//       "user.noOfSuccessfulExchanges": 10,
//       "user.ratingPositive": 10,
//       "user.ratingNegative": 1,
//       "user.participates": [
//         {
//           "user.participates|status": "completed",
//           "user.participates|type": "receiver",
//           "uid": "_:offer2"
//         }
//       ],
//       "user.game": [

//         {
//           "user.game|status": "has",
//           "tradeType": "sale",
//           "description": "",
//           "info": {
//             "uid": "_:game2"
//           }
//         },
//         {
//           "user.game|status": "has",
//           "tradeType": "sale",
//           "description": "",
//           "info": {
//             "uid": "_:game4"
//           }
//         }
//       ]
//     },
//     {
//       "user": "",
//       "uid": "_:user5",
//       "user.email": "omega@gmail.com",
//       "user.userName": "omega",
//       "user.description": "Omega omega",
//       "user.epochTimeCreated": "",
//       "user.isPro": false,
//       "user.matchNotifications": false,
//       "user.location": {
//         "type": "Point",
//         "coordinates": [
//           122.4220186,
//           37.772318
//         ]
//       },
//       "user.country": "NG",
//       "user.state": "FCT",
//       "user.noOfSuccessfulExchanges": 10,
//       "user.ratingPositive": 10,
//       "user.ratingNegative": 1,
//       "user.participates": [
//         {
//           "user.participates|status": "completed",
//           "user.participates|type": "receiver",
//           "uid": "_:offer2"
//         }
//       ],
//       "user.game": [

//         {
//           "user.game|status": "has",
//           "tradeType": "sale",
//           "description": "",
//           "info": {
//             "uid": "_:game3"
//           }
//         }
//       ]
//     }
//   ]
// }