import dgraph = require('dgraph-js')

export default async (client: dgraph.DgraphClient) => {
    const transaction = client.newTxn()
    const query = `
    {
        user(func: has(<user>)) {
          count: count(uid)
        }
      }

    `
    try {
        const json = (await transaction.query(query)).getJson()
        const result: number = json?.user[0]?.count
        return result
    } catch (error) {
        console.error(error)
        throw error
    } finally {
        transaction.discard()
    }
}