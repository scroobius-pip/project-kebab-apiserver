import dgraph = require('dgraph-js')
import { OfferStatus } from 'src/.generated/graphqlgen';
import { Offer } from 'src/types/models';
import parseDbOfferToOffer, { OfferDb } from './_utils/parseDbOfferToOffer';

export default async (
  status: OfferStatus,
  userId: string,
  pagination: { offset: number, limit: number } = { offset: 0, limit: 10 },
  client: dgraph.DgraphClient
) => {
  if (!userId.length) { throw new Error('userId Empty') }
  const transaction = client.newTxn()
  const query = `
    {
        var(func: uid(${userId})){
       offer AS  offers:user.participates @filter(eq(offer.status,"${status}"))
      }

        offers(func:uid(offer),orderdesc:offer.epochTimeCreated,first:${pagination.limit},offset:${pagination.offset}){
            uid
            epochTimeCreated:offer.epochTimeCreated
              offerId:offer.offerId
                status:offer.status
                receiverGames:offer.receiverGames {
                  description
                  tradeType
                  uid
                  info {
                    name:game.name
                    consoleType:game.platform
                    imageUrl:game.coverUrl
                  }
                }
                senderGames:offer.senderGames {
                  description
                  tradeType
                  uid
                  info {
                    name:game.name
                    consoleType:game.platform
                    imageUrl:game.coverUrl
                  }
                }
              receiverStatus:~user.participates @facets(eq(type,"receiver")) @facets(status:status){uid}
                  senderStatus:~user.participates @facets(eq(type,"sender")) @facets(status:status){uid}
        }
        total(func:uid(offer)){
          count(uid)
        }
      }
    `
  try {
    const json = (await transaction.query(query)).getJson()
    const offersDb: OfferDb[] = json.offers ? json.offers : []

    const result = {
      result: offersDb.map(offerDb => parseDbOfferToOffer(offerDb, userId)),
      pageInfo: {
        noOfItems: json.total ? json.total[0].count : 0
      }

    }

    return result
  } catch (error) {
    console.error(error)
    throw error
  } finally {
    transaction.discard()
  }
}
