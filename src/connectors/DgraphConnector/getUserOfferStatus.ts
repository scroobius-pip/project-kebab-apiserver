import { DgraphClient } from 'dgraph-js';

export default async (userId: string, offerId: string, client: DgraphClient) => {

    if (!userId.length) { throw new Error('userId Empty') }
    if (!offerId.length) { throw new Error('offerId Empty') }

    const transaction = client.newTxn()
    const query = `
    {
        user(func: uid(${userId})){
         user.participates @filter(uid(${offerId})) @facets(status:status)  {
          }
      }
      }
  `
    try {
        const json = (await transaction
            .query(query)).getJson()

        return json.user.length ? json.user[0]['user.participates'][0].status : null
    } catch (error) {
        console.error(error)
        throw error
    } finally {
        transaction.discard()
    }

}