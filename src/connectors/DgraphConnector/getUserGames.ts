import { UserGameDetailsStatus } from 'src/.generated/graphqlgen';
import { DgraphClient } from 'dgraph-js';
import parseDbUserGameToUserGame, { DbUserGame } from './_utils/parseDbUserGameToUserGame';
import { UserGame } from 'src/types/models';
const getUserGames = async (uid: string, status: UserGameDetailsStatus, client: DgraphClient): Promise<UserGame[]> => {
  const transaction = client.newTxn()
  const query = `
  {
    user(func: uid(${uid})){
      uid
      user.game @facets(status:status) @facets(eq(status, "${status}")) {
      description
      tradeType
      uid

      info {
        name:game.name
        imageUrl:game.coverUrl
        consoleType:game.platform
        id:uid
      }

      # user.game types below only used for custom game
      consoleType:platform
      name
    }
  }
}
  `
  try {
    const json = (await transaction
      .query(query)).getJson()

    const result: DbUserGame[] = json.user[0]['user.game'] ? json.user[0]['user.game'] : []

    return result.map((game) => parseDbUserGameToUserGame(game, status))
  } catch (error) {
    console.error(error)
    throw error
  } finally {
    transaction.discard()
  }


}

export default getUserGames