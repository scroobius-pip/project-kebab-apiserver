import { MutationResolvers } from 'src/.generated/graphqlgen';
import dgraph = require('dgraph-js')
import parse from './_utils/parseUserInfoInputToUserInfoDb';

export default async (
    info: MutationResolvers.UpdateUserInfoMutationInput['info'],
    userId: string,
    client: dgraph.DgraphClient
) => {
    if (!userId.length) { throw new Error('userId Empty') }
    const transaction = client.newTxn()

    try {
        const mutation = new dgraph.Mutation()
        const data = [{
            uid: userId,
            ...await parse(info)
        }]
        mutation.setSetJson(data)
        await transaction.mutate(mutation)
        await transaction.commit()
    } catch (error) {
        console.error(error)
        throw error
    } finally {
        transaction.discard()
    }
}