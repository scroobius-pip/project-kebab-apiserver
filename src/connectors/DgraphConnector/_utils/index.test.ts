import { expect } from 'chai'
import parseUserGameInputToAddGameDb from './parseUserGameInputToAddGameDb';
import { MutationResolvers } from 'src/.generated/graphqlgen';
import parseRemoveGameInputToRemoveGameDb from './parseRemoveGameInputToRemoveGameDb';
import parseUpdateGameInputToUpdateGameDb from './parseUpdateGameInputToUpdateGameDb';
import parseDbUserGameToUserGame, { DbUserGame } from './parseDbUserGameToUserGame';
import { Offer, UserGame, Match } from 'src/types/models';
import parseDbOfferToOffer, { OfferDb } from './parseDbOfferToOffer';
import parseUserInfoToUserInfoDb from './parseUserInfoInputToUserInfoDb';
import parseOfferToDbOffer, { offerQuery, offerInput } from './parseOfferToDbOffer';
import dgraph = require('dgraph-js');
import userExists from './userExists';
import dgraphInit from '../'
import offerExists from './offerExists';
import parseGameToUserGame from './parseOfferDbGameToUserGame';
import GameExists from './gameExists';
import parseDbMatchToMatch from './parseDbMatchToMatch';
describe('Connector Utils Tests', () => {
    describe('parseUserGameInputToAddGameDb should return correct output', () => {
        const games: MutationResolvers.AddGamesInput[] = [
            {
                details: {
                    description: 'usergame1details', status: 'want', tradeType: 'swap'
                },
                gameId: 'game1'
            },
            {
                details: {
                    description: 'usergame2details', status: 'has', tradeType: 'sale'
                },
                gameId: 'game2'
            },
            {
                details: {
                    description: 'second custom game added',
                    tradeType: 'sale',
                    status: 'want'
                },
                gameId: '',
                customItemDetails: {
                    consoleType: 'Custom Item',
                    name: 'Custom Item 2'
                }
            }

        ]

        const result = parseUserGameInputToAddGameDb(games, 'user1')

        it('Uid should be correct', () => {
            expect(result.uid).to.equal('user1')
        })

        it('output have same number of games as input ', () => {
            expect(result['user.game'].length).to.equal(games.length)
        })

        it('input should be contained in output', () => {
            const resultGameIdsOrName = result['user.game'].map((x) => x.info ? x.info.uid : x.name)
            const gameIdsOrName = games.map((x) => !!x.gameId ? x.gameId : x.customItemDetails.name)
            expect(resultGameIdsOrName).to.include.members(gameIdsOrName)
        })

        it('should accept custom games', () => {
            const customGames = result['user.game'].filter(game => {
                return !game.info
            })

            const customGamesNames = customGames.map(game => game.name)
            expect(customGamesNames).to.include('Custom Item 2')

        })

    })

    describe('parseRemoveGameInputToRemoveGameDb', () => {
        const games: MutationResolvers.RemoveGamesInput[] = [{ id: 'game1' }, { id: 'game2' }]
        const result: any = parseRemoveGameInputToRemoveGameDb(games, 'user1')

        it('Should return  correct result format', () => {
            const edgeRemove = result[0]
            expect(edgeRemove).to.deep.equal({
                'uid': 'user1',
                'user.game': [
                    { 'uid': 'game1' },
                    { 'uid': 'game2' }
                ]

            })

            const nodeRemove = result.slice(1)
            expect(nodeRemove.length).to.equal(2)
            expect(nodeRemove).to.deep.equal([
                { 'uid': 'game1' },
                { 'uid': 'game2' }
            ])
        })

    })

    describe('parseUpdateGameInputToUpdateGameDb', () => {
        const games: MutationResolvers.UpdateGamesInput[] = [
            { details: { description: 'game1description', status: 'has', tradeType: 'sale' }, id: 'game1' },
            { details: { description: 'game2description', status: 'want', tradeType: 'sale' }, id: 'game2' },
            { details: { description: 'game3description', status: 'want', tradeType: 'swap' }, id: 'game3' },

        ]
        const result = parseUpdateGameInputToUpdateGameDb(games, 'user1')
        it('Should return correct result format', () => {
            expect(result).to.deep.equal(
                [
                    {
                        uid: 'user1',
                        'user.game': [
                            {
                                'user.game|status': 'has',
                                uid: 'game1',
                                description: 'game1description',
                                tradeType: 'sale'
                            }, {
                                'user.game|status': 'want',
                                uid: 'game2',
                                description: 'game2description',
                                tradeType: 'sale',
                            }, {
                                'user.game|status': 'want',
                                uid: 'game3',
                                description: 'game3description',
                                tradeType: 'swap'
                            }
                        ]
                    }
                ]
            )
        })
    })

    describe('parseDbUserGameToUserGame', () => {

        const gamesHas: DbUserGame[] = [
            {
                description: 'description1', status: 'has', tradeType: 'sale', info: [{
                    consoleType: 'ps4',
                    id: 'game1',
                    imageUrl: 'game1image',
                    name: 'game1'
                }],
                uid: 'usergame1has'
            },
        ]
        const gamesWant: DbUserGame[] = [
            {
                description: 'description1', status: 'want', tradeType: 'sale', info: [{
                    consoleType: 'ps4',
                    id: 'game1',
                    imageUrl: 'game1image',
                    name: 'game1'
                }],
                uid: 'usergame1want'
            },

        ]

        const resultHas = gamesHas.map((game) => parseDbUserGameToUserGame(game, 'has'))
        const resultWant = gamesWant.map((game) => parseDbUserGameToUserGame(game, 'want'))

        it('Should return correct result format', () => {
            expect(resultHas).to.deep.equal([
                {
                    details: { tradeType: 'sale', status: 'has', description: 'description1' },
                    game: { name: 'game1', imageUrl: 'game1image', id: 'game1', consoleType: 'ps4' },
                    id: 'usergame1has'
                }
            ])

            expect(resultWant).to.deep.equal([
                {
                    details: { tradeType: 'sale', status: 'want', description: 'description1' },
                    game: { name: 'game1', imageUrl: 'game1image', id: 'game1', consoleType: 'ps4' },
                    id: 'usergame1want'
                }
            ])
        })
    })

    describe('parseDbOfferToOffer', () => {
        const dbOffer: OfferDb = {
            epochTimeCreated: 'epoch',
            receiverGames: [{
                uid: 'amomiUserGame',
                tradeType: 'sale',
                description: '',
                info: [{ consoleType: 'ps4', imageUrl: '', name: 'game2', uid: '' }]
            }],
            senderGames: [{
                uid: 'simdiUserGame',
                tradeType: 'swap',
                description: '',
                info: [{ consoleType: 'ps4', imageUrl: '', name: 'game1', uid: '', }]

            }],

            receiverStatus: [{ uid: 'amomi', status: 'completed' }],
            senderStatus: [{ uid: 'simdi', status: 'ongoing' }],
            status: 'ongoing',
            uid: 'offerId'
        }

        const result = parseDbOfferToOffer(dbOffer, 'simdi')

        const expectedResult: Offer = {
            epochTimeCreated: 'epoch',
            senderGames: [{
                details: {
                    description: '',
                    tradeType: 'swap',
                },
                game: {
                    consoleType: 'ps4',
                    id: '',
                    imageUrl: '',
                    name: 'game1',
                },
                id: 'simdiUserGame'
            }],
            receiverGames: [{
                details: {
                    description: '',
                    tradeType: 'sale',
                },
                game: {
                    consoleType: 'ps4',
                    id: '',
                    imageUrl: '',
                    name: 'game2',
                },
                id: 'amomiUserGame'
            }],
            offerId: 'offerId',
            senderId: 'simdi',
            receiverId: 'amomi',
            receiverStatus: 'completed',
            senderStatus: 'ongoing',
            status: 'ongoing',
        }

        it('Should return correct result format', () => {
            expect(result).to.deep.equal(expectedResult)
        })

    })

    describe('parseUserInfoToUserInfoDb', () => {
        it('Should not return missing fields', async () => {
            const userInfoDb = await parseUserInfoToUserInfoDb({ description: 'thanks', email: 'sim04ful@gmail.com' })
            expect(userInfoDb)
                .to
                .not
                .have
                .keys(['user.location',
                    'user.userImageUrl', 'user.userName', 'user.isPro', 'user.noOfSuccessfulExchanges'])

            expect(userInfoDb)
                .to
                .have
                .keys(['user.description', 'user.email'])
        })

        it('Should not return extreneous fields', async () => {
            const userInfoDb = await parseUserInfoToUserInfoDb({ description: 'thanks', email: 'sim04ful@gmail.com' })
            expect(Object.keys(userInfoDb).length).to.be.equal(2)
        })

        it('Should return correct data', async () => {
            const userInfoDb = await parseUserInfoToUserInfoDb({
                description: 'thanks',
                email: 'sim04ful@gmail.com',
                isBanned: false,
                isPro: true,
                setting_matchNotifications: true,
                noOfSuccessfulExchanges: 12,
                rating: {
                    negative: 1,
                    positive: 30
                },
                location: {
                    latitude: 9,
                    longitude: 7
                },
                userImageUrl: 'imageUrl',
                userName: 'sim04ful',
                user_subscriptionCancelUrl: 'cancel_url',
                user_subscriptionUpdateUrl: 'update_url'
            })

            expect(userInfoDb).to.be.deep.equal({
                'user.description': 'thanks',
                'user.email': 'sim04ful@gmail.com',
                'user.userImageUrl': 'imageUrl',
                'user.userName': 'sim04ful',
                'user.isPro': true,
                'user.isBanned': false,
                'user.noOfSuccessfulExchanges': 12,
                'user.ratingNegative': 1,
                'user.ratingPositive': 30,
                'user.matchNotifications': true,
                'user.state': 'FCT',
                'user.country': 'NG',
                'user.location': {
                    'coordinates': [
                        7,
                        9
                    ],
                    'type': 'Point'
                },
                'user.latitude': 9,
                'user.longitude': 7,
                'user.subscriptionCancelUrl': 'cancel_url',
                'user.subscriptionUpdateUrl': 'update_url'
            })
        })
    })

    describe('parseOfferToDbOffer', () => {
        it('should return correct data', () => {
            const timeNow = (new Date()).toISOString()
            const input: offerInput = {
                myGames: [{
                    gameId: '0xa1',
                    details: {
                        description: 'changed',
                        tradeType: 'swap'
                    }
                }],
                otherGames: [{
                    gameId: '0xa3',
                    details: {
                        description: '',
                        tradeType: 'swap'
                    }
                }],
                otherId: '0xc2',
                epochTimeNow: timeNow
            }

            const output = parseOfferToDbOffer(input, '0xc1')

            const expectedOutput: offerQuery = [
                {
                    uid: '_:offer',
                    offer: '',
                    'offer.status': 'pending',
                    'offer.epochTimeCreated': timeNow,
                    'offer.offerId': '0xc10xc2',
                    'offer.receiverGames': [
                        {
                            'tradeType': 'swap',
                            description: '',
                            info: {
                                uid: '0xa3'
                            }
                        }
                    ],
                    'offer.senderGames': [
                        {
                            tradeType: 'swap',
                            description: 'changed',
                            info: {
                                uid: '0xa1'
                            }
                        }
                    ],
                },
                {
                    uid: '0xc1',
                    'user.participates':
                    {
                        'user.participates|status': 'ongoing',
                        'user.participates|type': 'sender',
                        uid: '_:offer'
                    }

                }, {
                    uid: '0xc2',
                    'user.participates':
                    {
                        'user.participates|status': 'pending',
                        'user.participates|type': 'receiver',
                        uid: '_:offer'
                    }

                }

            ]

            expect(output).to.deep.equal(expectedOutput)
        })
    })

    describe('userExists', () => {
        const connector = dgraphInit()

        beforeEach(async () => {
            await connector.clearDb()
            await connector.seedDb()
        })

        it('should return true if user exists', async () => {
            const clientStub = new dgraph.DgraphClientStub()
            const client = new dgraph.DgraphClient(clientStub)
            const transaction = client.newTxn()
            const result = await userExists('sim04ful@gmail.com', transaction)
            expect(result).to.be.true
        })

        it('should return false if user not exist', async () => {
            const clientStub = new dgraph.DgraphClientStub()
            const client = new dgraph.DgraphClient(clientStub)
            const transaction = client.newTxn()
            const result1 = await userExists('notexists@gmail.com', transaction)
            expect(result1).to.be.false
        })
    })

    describe('gameExists', () => {
        const connector = dgraphInit()

        beforeEach(async () => {
            await connector.clearDb()
            await connector.seedDb()
        })

        it('should return true if game exists', async () => {
            const clientStub = new dgraph.DgraphClientStub()
            const client = new dgraph.DgraphClient(clientStub)
            const transaction = client.newTxn()
            const result = await GameExists('Remnant From The Ashes', 'Linux', transaction)
            expect(result).to.be.true

        })

        it('should return false if game not exist', async () => {
            const clientStub = new dgraph.DgraphClientStub()
            const client = new dgraph.DgraphClient(clientStub)
            const transaction = client.newTxn()
            const result1 = await GameExists('doesnt exist', 'Linux', transaction)
            const result2 = await GameExists('Remnant From The Ashes', 'Windows', transaction)
            expect(result1).to.be.false
            expect(result2).to.be.false
        })
    })

    describe('offerExists', () => {
        const connector = dgraphInit()

        beforeEach(async () => {
            await connector.clearDb()
            await connector.seedDb()
        })

        it('should return uid if offer exists', async () => {
            const clientStub = new dgraph.DgraphClientStub()
            const client = new dgraph.DgraphClient(clientStub)
            const transaction = client.newTxn()
            const uid = await offerExists('0xc10xc3', transaction)
            expect(uid).to.be.equal('0xb2')
        })

        it('should return empty string if offer not exist', async () => {
            const clientStub = new dgraph.DgraphClientStub()
            const client = new dgraph.DgraphClient(clientStub)
            const transaction = client.newTxn()
            const uid = await offerExists('0xa10xc28', transaction)
            expect(uid).to.be.equal('')
        })

    })

    describe('parseGameToUserGame', () => {
        const result = parseGameToUserGame({
            description: 'description',
            info: [{
                consoleType: 'playstation',
                name: 'batman',
                imageUrl: 'imageurl',
                uid: '0xd3'
            }],
            tradeType: 'sale',
            uid: '0xb3',

        })

        const expectedResult: UserGame = {
            details: {
                description: 'description',
                tradeType: 'sale',
            },
            game: {
                consoleType: 'playstation',
                name: 'batman',
                id: '0xd3',
                imageUrl: 'imageurl'
            },
            id: '0xb3'
        }

        it('should return correct data', () => {
            expect(result).to.deep.equal(expectedResult)
        })
    })

    describe('parseDbMatchToMatch', () => {




        it('should return correct data', () => {
            const result = parseDbMatchToMatch({
                'uid': '0xc2',
                'user.userName': 'amomi',
                'user.userImageUrl': 'url',
                'user.country': 'NG',
                'user.state': 'FCT',
                'score': 2,
                'user.game': [
                    {
                        'info': [
                            {
                                'has': 'New Super Mario Bros. Deluxe!'
                            }
                        ]
                    },
                    {
                        'info': [
                            {
                                'wants': 'Destiny Warriors RPG'
                            }
                        ]
                    },
                    {
                        'info': [
                            {
                                'has': 'Remnant From The Ashes'
                            }
                        ]
                    }
                ]
            })
            const expectedResult: Match = {
                hasGameNames: ['New Super Mario Bros. Deluxe!', 'Remnant From The Ashes'],
                wantedGameNames: ['Destiny Warriors RPG'],
                id: '0xc2',
                userImageUrl: 'url',
                userName: 'amomi',
                state: 'FCT',
                country: 'NG'
            }
            expect(result).to.deep.equal(expectedResult)
        })

        it('should return correct data when user.game does not exist', () => {
            const result = parseDbMatchToMatch({
                'uid': '0xc2',
                'user.userName': 'amomi',
                'user.userImageUrl': 'url',
                'user.country': 'NG',
                'user.state': 'FCT',
                'score': 2,
            })

            const expectedResult: Match = {
                hasGameNames: [],
                wantedGameNames: [],
                id: '0xc2',
                userImageUrl: 'url',
                userName: 'amomi',
                state: 'FCT',
                country: 'NG'
            }

            expect(result).to.deep.equal(expectedResult)
        })
        it('should not contain duplicates', () => {
            const result = parseDbMatchToMatch({
                "uid": "0xc2",
                "user.userName": "amomi",
                "user.userImageUrl": "",
                "user.country": "NG",
                "user.state": "FCT",
                "score": 4,
                "user.game": [
                    {
                        "info": [
                            {
                                "wants": "Destiny Warriors RPG"
                            }
                        ]
                    },
                    {
                        "info": [
                            {
                                "has": "New Super Mario Bros. Deluxe!"
                            }
                        ]
                    },
                    {
                        "info": [
                            {
                                "has": "New Super Mario Bros. Deluxe!"
                            }
                        ]
                    },
                    {
                        "info": [
                            {
                                "wants": "Destiny Warriors RPG"
                            }
                        ]
                    }
                ]
            })
            expect(result.hasGameNames.length).to.equal(1)
            expect(result.wantedGameNames.length).to.equal(1)
        })

    })

})
