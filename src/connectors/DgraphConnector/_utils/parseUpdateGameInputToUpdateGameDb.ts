import { MutationResolvers, UserGameDetailsStatus, UserGameDetailsTradeType } from 'src/.generated/graphqlgen';

export interface UpdateGameDb {
    uid: string // id of user node
    'user.game': Array<{
        'user.game|status': UserGameDetailsStatus
        uid: string // id of usergame node
        tradeType: UserGameDetailsTradeType
        description: string
    }>
}

export default (games: MutationResolvers.UpdateGamesInput[], userId: string): UpdateGameDb[] => {
    return [
        {
            uid: userId,
            'user.game': games.map(game => ({
                'user.game|status': game.details.status,
                uid: game.id,
                tradeType: game.details.tradeType,
                description: game.details.description
            }))
        }]

}