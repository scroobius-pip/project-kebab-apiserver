import { User } from 'src/types/models';

export interface DbUser {
    uid: string
    'user.email': string
    'user.userName'?: string
    'user.epochTimeCreated'?: string
    'user.isPro'?: boolean
    'user.location'?: {
        type: string,
        coordinates: [number, number] | [0, 0],
    }
    'user.noOfSuccessfulExchanges'?: number
    'user.ratingNegative'?: number
    'user.ratingPositive'?: number
    'user.userImageUrl'?: string
    'user.description'?: string
    'user.isBanned'?: boolean
    'user.matchNotifications'?: boolean
    'user.country'?: string
    'user.state'?: string
    'user.subscriptionUpdateUrl'?: string
    'user.subscriptionCancelUrl'?: string

}

export default (dbUser: DbUser): User => {
    const {
        uid,
        'user.email': email,
        'user.userName': userName = '',
        'user.epochTimeCreated': epochTimeCreated,
        'user.isPro': isPro = false,
        'user.noOfSuccessfulExchanges': noOfSuccessfulExchanges,
        'user.ratingNegative': ratingNegative,
        'user.ratingPositive': ratingPositive,
        'user.userImageUrl': userImageUrl,
        'user.description': description,
        'user.isBanned': isBanned = false,
        'user.country': country = '',
        'user.state': state = '',
        // tslint:disable-next-line: variable-name
        'user.subscriptionCancelUrl': user_subscriptionCancelUrl = '',
        'user.subscriptionUpdateUrl': user_subscriptionUpdateUrl = '',
        // tslint:disable-next-line: variable-name
        'user.matchNotifications': setting_matchNotifications = false,

    } = dbUser

    return {
        id: uid,
        info: {
            description,
            email,
            epochTimeCreated,
            isPro,
            location: {
                country,
                state
            },
            noOfSuccessfulExchanges,
            rating: {
                positive: ratingPositive,
                negative: ratingNegative
            },
            userImageUrl,
            userName,
            isBanned,
            setting_matchNotifications,
            user_subscriptionCancelUrl,
            user_subscriptionUpdateUrl
        }
    }
}
