import { MutationResolvers } from 'src/.generated/graphqlgen';

export type RemoveGameDb = Array<({
    'uid': string; //user's uid
    'user.game': Array<{ // deleting user.game edge
        'uid': string;
    }>
} | {
    'uid': string; // user.game uid
    'user.game'?: undefined;
})>

export default (games: MutationResolvers.RemoveGamesInput[], userId: string): RemoveGameDb => {
    const data: RemoveGameDb = [
        {
            uid: userId,
            'user.game': []
        }
    ]

    games.forEach(game => {
        data[0]['user.game'].push({ uid: game.id })
        data.push({ uid: game.id })
    })

    return data
}
