import { OfferDbGame } from './parseDbOfferToOffer';
import { UserGame } from 'src/types/models';

export default (game: OfferDbGame): UserGame => {
    const { consoleType, imageUrl, name } = game.info[0]
    const { description, tradeType } = game
    const result = {
        id: game.uid,
        game: {
            consoleType,
            imageUrl,
            name,
            id: game.info[0].uid,
        },
        details: {
            description,
            tradeType,
        }
    }
    return result
}
