import { MutationResolvers, UserGameDetailsTradeType, OfferStatus } from 'src/.generated/graphqlgen';

export type offerInput = MutationResolvers.CreateOfferMutationInput['offer'] & { epochTimeNow: string }

interface OfferDbGameOutput {
    tradeType: UserGameDetailsTradeType;
    description: string;
    info: {
        uid: string;
    };
}

interface OfferDbOutput {
    uid: string;
    offer: '';
    'offer.offerId': string;
    'offer.status': OfferStatus;
    'offer.epochTimeCreated': string;
    'offer.senderGames': OfferDbGameOutput[];
    'offer.receiverGames': OfferDbGameOutput[];
}

interface SenderUserParticipate {
    uid: string,
    'user.participates': {
        'user.participates|status': OfferStatus,
        'user.participates|type': 'sender',
        uid: string
    },

}
interface ReceiverUserParticipate {
    uid: string,
    'user.participates': {
        'user.participates|status': OfferStatus,
        'user.participates|type': 'receiver',
        uid: string
    },

}

export type offerQuery = [
    OfferDbOutput,
    SenderUserParticipate,
    ReceiverUserParticipate
]

export const createOfferIdFromUserIds = (userIds: string[]) => {
    return userIds.sort().join('')
}

const userGameInputToOfferGame = (game: MutationResolvers.UserGameInput): OfferDbGameOutput => {
    const { gameId, details: { description, tradeType } } = game
    return {
        description,
        info: {
            uid: gameId
        },
        tradeType
    }
}

export default (offer: offerInput, userId: string): offerQuery => {
    const { epochTimeNow, otherId, otherGames, myGames } = offer
    return [
        {
            uid: '_:offer',
            'offer': '',
            'offer.offerId': createOfferIdFromUserIds([userId, otherId]),
            'offer.status': 'pending',
            'offer.epochTimeCreated': epochTimeNow,
            'offer.senderGames': myGames.map(userGameInputToOfferGame),
            'offer.receiverGames': otherGames.map(userGameInputToOfferGame),
        },
        {
            uid: userId,
            'user.participates': {
                'user.participates|status': 'ongoing',
                'user.participates|type': 'sender',
                uid: '_:offer'
            }
        },
        {
            uid: otherId,
            'user.participates': {
                'user.participates|status': 'pending',
                'user.participates|type': 'receiver',
                uid: '_:offer'
            }
        }
    ]

}