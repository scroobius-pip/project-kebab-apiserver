import dgraph = require('dgraph-js');

export default async function offerExists(offerId: string, transaction: dgraph.Txn): Promise<string> {

    const offerQuery = `query offer($offerId: string){
        offer(func: eq(offer.offerId, $offerId)){
            uid
        }
    }`;
    const vars = {
        $offerId: offerId
    };
    const offer = (await transaction.queryWithVars(offerQuery, vars)).getJson().offer[0];
    return !!(offer && 'uid' in offer) ? offer.uid : ''

}
