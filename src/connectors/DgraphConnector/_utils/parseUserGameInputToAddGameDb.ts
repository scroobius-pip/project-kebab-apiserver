import { MutationResolvers, UserGameDetailsTradeType, UserGameDetailsStatus } from 'src/.generated/graphqlgen';

export interface AddGameDb {
    uid: string;
    'user.game': Array<{
        'user.game|status': UserGameDetailsStatus
        description: string;
        tradeType: UserGameDetailsTradeType;
        name?: string
        platform?: string
        info?: {
            uid: string;
        };
    }>,

}

export default (games: MutationResolvers.AddGamesInput[], userId: string): AddGameDb => {
    return {
        'uid': userId,
        'user.game': games.map(game => {
            return {
                'user.game|status': game.details.status,
                description: game.details.description,
                tradeType: game.details.tradeType,
                ...(game.customItemDetails ? {
                    name: game.customItemDetails.name,
                    platform: game.customItemDetails.consoleType
                } :
                    {
                        info: {
                            uid: game.gameId
                        }
                    }

                ),

            }
        }),
    }
}