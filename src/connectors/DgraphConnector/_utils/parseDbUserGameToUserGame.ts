import { UserGame } from 'src/types/models';
import { UserGameDetailsTradeType, UserGameDetailsStatus } from 'src/.generated/graphqlgen';

export interface DbUserGame {
    uid: string
    description: string
    tradeType: UserGameDetailsTradeType
    status: UserGameDetailsStatus
    consoleType?: string
    name?: string
    info?: DbUserGameInfo[]

}

export interface DbUserGameInfo {
    'name': string
    id: string
    'imageUrl': string
    'consoleType': string

}

const parseDbUserGameToUserGame = (dbUserGame: DbUserGame, status: 'has' | 'want'): UserGame => {
    const {
        uid: id,
        description,
        name = '',
        consoleType = '',
        info,
        tradeType
    } = dbUserGame

    if (!info) {
        //hello
        console.log(dbUserGame)
    }

    return {
        id,
        details: {
            description,
            status,
            tradeType
        },
        game: {
            name: name || info[0].name,
            consoleType: consoleType || info[0].consoleType,
            id: name && consoleType ? name + consoleType : info[0].id,
            imageUrl: name ? '' : info[0].imageUrl,
        }
    }
}

export default parseDbUserGameToUserGame