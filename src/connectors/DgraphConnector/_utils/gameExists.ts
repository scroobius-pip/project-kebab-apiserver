import dgraph = require('dgraph-js')

export default async (gameName: string, consoleType: string, transaction: dgraph.Txn): Promise<boolean> => {
    try {
        const gameQuery = `
    query game($uuid: string){
        game(func: eq(game.uuid, $uuid)){
            uid
      }
    }`
        const vars = {
            $uuid: (gameName + consoleType).replace(/\s/g, '')
        }
        const game = (await transaction.queryWithVars(gameQuery, vars)).getJson().game[0]
        return !!(game && 'uid' in game)
    } catch (error) {
        console.error(error)
        throw error
    }
}