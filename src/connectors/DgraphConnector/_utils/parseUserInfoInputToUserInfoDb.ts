import { MutationResolvers, QueryResolvers } from 'src/.generated/graphqlgen';
// tslint:disable-next-line: no-var-requires
const renameObjectKeys = require('object-rename-keys')
import { UserInfo } from 'src/types/models';
import axios from 'axios'
export interface UserInfoDb {
    'user.description'?: string
    'user.email'?: string
    'user.location'?: {
        'type': 'Point',
        'coordinates': [number, number]
    }
    'user.longitude'?: number
    'user.latitude'?: number
    'user.country'?: string
    'user.state'?: string
    'user.userImageUrl'?: string
    'user.userName'?: string
    'user.isPro'?: boolean
    'user.isBanned'?: boolean
    'user.noOfSuccessfulExchanges'?: number
    'user.ratingNegative'?: number
    'user.ratingPositive'?: number
    'user.matchNotifications'?: boolean
    'user.subscriptionUpdateUrl'?: string
    'user.subscriptionCancelUrl'?: string
}

const propertyMap = {
    'email': 'user.email',
    'description': 'user.description',
    'userImageUrl': 'user.userImageUrl',
    'userName': 'user.userName',
    'epochTimeCreated': 'user.epochTimeCreated',
    'noOfSuccessfulExchanges': 'user.noOfSuccessfulExchanges',
    'isPro': 'user.isPro',
    'isBanned': 'user.isBanned',
    'ratingNegative': 'user.ratingNegative',
    'ratingPositive': 'user.ratingPositive',
    'location': 'user.location',
    'longitude': 'user.longitude',
    'latitude': 'user.latitude',
    'setting_matchNotifications': 'user.matchNotifications',
    'country': 'user.country',
    'state': 'user.state',
    'user_subscriptionUpdateUrl': 'user.subscriptionUpdateUrl',
    'user_subscriptionCancelUrl': 'user.subscriptionCancelUrl'
}

export default async (input: MutationResolvers.UserInfoInput & {
    rating?: UserInfo['rating'],
    noOfSuccessfulExchanges?: UserInfo['noOfSuccessfulExchanges']
}): Promise<UserInfoDb> => {
    const { location, rating, ...rest } = input
    const result = {
        ...rest,
        ...(location ? await LocationObject(location) : {}),
        ...(rating ? {
            ratingPositive: rating.positive,
            ratingNegative: rating.negative
        } : {})
    }
    return renameObjectKeys(result, propertyMap)
}

const LocationObject = async (location: MutationResolvers.UserInfoLocationInput): Promise<{
    location: UserInfoDb['user.location'],
    longitude: UserInfoDb['user.longitude'],
    latitude: UserInfoDb['user.latitude'],
    country: string,
    state: string
}> => {
    if (location.latitude === 0) {
        return {
            location: {
                type: 'Point',
                coordinates: [82.8628, 135.0000] // antarctica
            },
            longitude: 82.8628,
            latitude: 135.0000,
            state: '',
            country: ''
        }
    }

    const IQ_KEY = process.env.LOCATION_IQ_KEY || '8c845b5c6e0abb'
    if (!IQ_KEY) throw new Error('Location iq key not specified')
    const { longitude: long, latitude: lat } = location
    // tslint:disable-next-line: max-line-length
    const url = `https://us1.locationiq.com/v1/reverse.php?key=${IQ_KEY}&lat=${lat}&lon=${long}&format=json&zoom=5&namedetails=1`
    const result = await axios.get<LocationIQResult>(url)
    const iqLocation = result.data
    return {
        location: {
            type: 'Point',
            coordinates: [long, lat]
        },
        longitude: long,
        latitude: lat,
        state: iqLocation.namedetails.ref ? iqLocation.namedetails.ref.toUpperCase() : iqLocation.address.state,
        country: iqLocation.address.country_code.toUpperCase()
    }
}

export interface LocationIQResult {
    place_id: string;
    licence: string;
    osm_type: string;
    osm_id: string;
    lat: string;
    lon: string;
    display_name: string;
    address: Address;
    namedetails: { ref?: string, name?: string, [key: string]: string };
    boundingbox: string[];
}

export interface Address {
    state: string;
    country: string;
    country_code: string;
}
