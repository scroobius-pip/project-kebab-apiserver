import { Offer, OfferStatus } from 'src/types/models';
import { UserGameDetailsTradeType } from 'src/.generated/graphqlgen';
import parseGameToUserGame from './parseOfferDbGameToUserGame';

export interface OfferDbGame {
    'description': string;
    'tradeType': UserGameDetailsTradeType;
    'uid': string;
    'info': Array<{
        'name': string;
        'consoleType': string;
        'imageUrl': string;
        'uid': string;
    }>;
}

export interface OfferDb {

    'uid': string;
    'epochTimeCreated': string;
    'offer.offerId'?: string
    'status': OfferStatus;
    'receiverGames': OfferDbGame[];
    'senderGames': OfferDbGame[];
    'receiverStatus': Array<{
        'status': OfferStatus;
        'uid': string;
    }>
    'senderStatus': Array<{
        'status': OfferStatus;
        'uid': string;
    }>

}

const parseDbOfferToOffer = (DbOffer: OfferDb, userId: string): Offer => {

    const { uid: offerId, epochTimeCreated, receiverGames, receiverStatus, senderGames, senderStatus, status } = DbOffer
    const { uid: senderId, } = senderStatus[0]
    const { uid: receiverId, } = receiverStatus[0]

    return {
        status,
        epochTimeCreated,
        offerId,
        senderStatus: senderStatus[0].status,
        receiverStatus: receiverStatus[0].status,
        receiverGames: receiverGames.map(parseGameToUserGame),
        senderGames: senderGames.map(parseGameToUserGame),
        receiverId,
        senderId
    }
}

export default parseDbOfferToOffer