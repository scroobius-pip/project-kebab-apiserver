import dgraph = require('dgraph-js');

export default async function userExists(userEmail: string, transaction: dgraph.Txn) {

  const emailQuery = `query user($email: string){
            user(func: eq(user.email, $email)){
              uid
            }
          }`;
  const vars = {
    $email: userEmail
  };
  const user = (await transaction.queryWithVars(emailQuery, vars)).getJson().user[0];
  return !!(user && 'uid' in user)

}
