import { MutationResolvers } from 'src/.generated/graphqlgen';

function generateGameDbUuid(gameName: string, consoleType: string): string {
    return (gameName + consoleType).replace(/\s/g, '');
}

export default generateGameDbUuid