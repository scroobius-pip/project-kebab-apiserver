import { Connector } from 'src/connectors';

const getAllUserGames = async (uid: string, connector: Connector) => {
    const results = await Promise.all([
        connector.getUserGames(uid, 'want'),
        connector.getUserGames(uid, 'has')
    ])
    const result = [...results[0], ...results[1]]
    return result
}

export default getAllUserGames