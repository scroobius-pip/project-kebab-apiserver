import dgraph = require('dgraph-js')

export default async (offerId: string, userId: string, client: dgraph.DgraphClient) => {
    const transaction = client.newTxn()
    const participantsQuery = `
{
    participants(func: uid(${offerId}))@normalize{
    ~user.participates {
      uid
}}}`
    try {
        const json = (await transaction.query(participantsQuery)).getJson()
        const result: Array<{ uid: string }> = json.participants
        const participantsUids = result.map(p => p.uid)
        return participantsUids.includes(userId)
    } catch (error) {
        console.error(error)
        throw error
    } finally {
        transaction.discard()
    }
}