import { Connector } from 'src/connectors'
import dgraph = require('dgraph-js')
import grpc = require('@grpc/grpc-js')
import clearDb from './clearDb'
import createUser from './createUser'
import getUser from './getUser'
import seedDb from './seedDb'
import getUserGames from './getUserGames'
import addUserGames from './addUserGames'
import removeGames from './removeUserGames'
import updateGames from './updateUserGames'
import setUserOfferStatus from './setUserOfferStatus'
import getUserOfferStatus from './getUserOfferStatus'
import updateOffer from './updateOffer'
import getOffer from './getOffer'
import updateUser from './updateUser'
import getUserOffers from './getUserOffers';
import createOffer from './createOffer';
import createDbGames from './createDbGames';
import getNoOfUserGames from './getNoOfUserGames';
import participatesInOffer from './participatesInOffer';
import searchGames from './searchGames';
import getMatches from './getMatches';
import getUserCount from './getUserCount'

type DgraphConnectorType = (address?: string) => Connector

const initDgraphClient = (address: string) => {
  try {
    const clientStub = new dgraph.DgraphClientStub(address,
      grpc.credentials.createInsecure())
    const client = new dgraph.DgraphClient(clientStub)
    // client.setDebugMode(true)
    return client
  } catch (error) {
    console.error(error)
    throw error
  }
}
const DgraphConnector: DgraphConnectorType = (address?: string) => {
  if (!address) { address = 'localhost:9080' }
  const client = initDgraphClient(address)

  return {
    seedDb: () => seedDb(client),
    clearDb: () => clearDb(client),
    createUser: (userId) => createUser(userId, client),
    getUser: {
      withUserEmail: (userEmail) => getUser.withUserEmail(userEmail, client),
      withUserName: (userName) => getUser.withUserName(userName, client),
      withUid: (uid) => getUser.withUid(uid, client)
    },
    getUserGames: (userId, status) => getUserGames(userId, status, client),
    addUserGames: (games, userId) => addUserGames(games, userId, client),
    removeUserGames: (games, userId) => removeGames(games, userId, client),
    updateUserGames: (games, userId) => updateGames(games, userId, client),
    getUserOfferStatus: (userId, offerId) => getUserOfferStatus(userId, offerId, client),
    setUserOfferStatus: (userId, userType, offerId, status) =>
      setUserOfferStatus(userId, userType, offerId, status, client),
    updateOffer: (offerId, status) => updateOffer(offerId, status, client),
    getOffer: (uid, userId) => getOffer(uid, userId, client),
    updateUser: (info, userId) => updateUser(info, userId, client),
    getUserOffers: (status, userId, pagination) => getUserOffers(status, userId, pagination, client),
    createOffer: (offer, userId) => createOffer(offer, userId, client),
    createDbGames: (games) => createDbGames(games, client),
    getNoOfUserGames: (userId, status) => getNoOfUserGames(userId, status, client),
    participatesInOffer: (offerId, userId) => participatesInOffer(offerId, userId, client),
    searchDbGames: (searchString, limit) => searchGames(searchString, limit, client),
    getMatches: {
      byLocation: (userId) => getMatches.byLocation(userId, client),
      byMatchRate: (userId) => getMatches.byMatchRate(userId, client)
    },
    getUserCount: () => getUserCount(client)
  }
}

export default DgraphConnector