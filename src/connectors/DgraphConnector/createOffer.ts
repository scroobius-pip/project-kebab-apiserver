import dgraph = require('dgraph-js')
import { MutationResolvers } from 'src/.generated/graphqlgen';
import parseOfferToDbOffer, { createOfferIdFromUserIds } from './_utils/parseOfferToDbOffer';
import offerExists from './_utils/offerExists';

async function deleteOffer(
    offerUid: string,
    userId: string,
    offer: MutationResolvers.OfferInput,
    transaction: dgraph.Txn) {
    const deleteEdgeMutation = new dgraph.Mutation();
    deleteEdgeMutation.setDeleteJson([{
        uid: offerUid,
    }, {
        uid: userId,
        'user.participates': {
            uid: offerUid
        }
    }, {
        uid: offer.otherId,
        'user.participates': {
            uid: offerUid
        }
    }]);
    await transaction.mutate(deleteEdgeMutation);
}


export default async (
    offer: MutationResolvers.CreateOfferMutationInput['offer'],
    userId: string,
    client: dgraph.DgraphClient
) => {
    if (!userId.length) { throw new Error(`userId Empty`) }
    const transaction = client.newTxn()
    try {
        const offerId = createOfferIdFromUserIds([userId, offer.otherId])
        const offerUid = await offerExists(offerId, transaction)
        if (offerUid) {
            await deleteOffer(offerUid, userId, offer, transaction);
        }
        const offerData = parseOfferToDbOffer(
            { ...offer, epochTimeNow: (new Date()).toISOString() },
            userId,
        )

        const mutation = new dgraph.Mutation()
        mutation.setSetJson(offerData)
        const result: any = await transaction.mutate(mutation)
        await transaction.commit()

        return { uid: result.getUidsMap().get('offer') }
    } catch (error) {
        console.error(error)
        throw error
    } finally {
        transaction.discard()
    }
}
