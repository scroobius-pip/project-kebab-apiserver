import dgraph = require('dgraph-js')
import { UserGameDetailsStatus } from 'src/.generated/graphqlgen';

export default async (
    userId: string,
    status: UserGameDetailsStatus,
    client: dgraph.DgraphClient
) => {
    const transaction = client.newTxn()
    const query = `
    {
        total(func: uid(${userId})) @normalize {
            user.game @facets(eq(status, "${status}")){
                gameCount: count(uid)
     }}}
    `
    try {
        const json = (await transaction.query(query)).getJson()
        const result: number = json.total[0] ? json.total[0].gameCount : 0
        return result
    } catch (error) {
        console.error(error)
        throw error
    } finally {
        transaction.discard()
    }

}