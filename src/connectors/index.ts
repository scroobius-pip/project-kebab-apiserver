import {
  User,
  OfferQueryResult,
  UserGame,
  Offer,
  Game,
  Match
} from 'src/types/models'
import {
  MutationResolvers,
  QueryResolvers,
  UserGameDetailsStatus,
  OfferStatus} from 'src/.generated/graphqlgen'

export class Connector {
  public createUser?: (userEmail: string) => Promise<{ uid: string }>
  public addUserGames?: (
    games: MutationResolvers.AddGamesMutationInput['games'],
    uid: string
  ) => Promise<void>
  public removeUserGames?: (
    games: MutationResolvers.RemoveGamesMutationInput['games'],
    uid: string
  ) => Promise<void>
  public updateUserGames?: (
    games: MutationResolvers.UpdateGamesMutationInput['games'],
    userId: string
  ) => Promise<void>
  public getMatches?: {
    byLocation: (userId: string) => Promise<Match[]>
    byMatchRate: (userId: string) => Promise<Match[]>
  }
  public updateOffer?: (offerId: string, status: OfferStatus) => Promise<void>
  public createOffer?: (
    offer: MutationResolvers.CreateOfferMutationInput['offer'],
    userId: string
  ) => Promise<{ uid: string }>
  public deleteOffer?: (offerId: string) => Promise<void>
  public getUserOffers?: (
    status: OfferStatus,
    userId: string,
    pagination: QueryResolvers.PaginationInput
  ) => Promise<OfferQueryResult>
  public getOffer?: (offerId: string, userId: string) => Promise<Offer | null>
  public getUser?: {
    withUid: (uid: string) => Promise<User | null>
    withUserName: (userName: string) => Promise<User | null>
    withUserEmail: (userEmail: string) => Promise<User | null>
  }
  public getUserGames?: (userId: string, status: UserGameDetailsStatus) => Promise<UserGame[]>
  public updateUser?: (
    info: MutationResolvers.UpdateUserInfoMutationInput['info'],
    userId: string
  ) => Promise<void>
  public participatesInOffer?: (offerId: string, userId: string) => Promise<boolean>
  public getNoOfUserGames?: (userId: string, status: 'has' | 'want') => Promise<number>
  public clearDb?: () => Promise<void>
  public seedDb?: () => Promise<void>
  public getUserOfferStatus?: (userId: string, offerId: string) => Promise<OfferStatus | null>
  public setUserOfferStatus?: (
    userId: string, type: 'receiver' | 'sender',
    offerId: string, status: OfferStatus) => Promise<OfferStatus>
  public createDbGames?: (games: MutationResolvers.AddDbGamesMutationInput['games']) =>
    Promise<MutationResolvers.AddDbGamesMutationInput['games']>
  public searchDbGames?: (searchText: string, limit: number) => Promise<Game[]>
  public getUserCount?: () => Promise<number>

}
